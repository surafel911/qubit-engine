CPP 		:= clang++
CPPFLAGS	:= -std=c++17 -Wall -target x86_64-pc-windows-gnu
MACROS		:= $(addprefix -D ,GLFW_INCLUDE_NONE _USE_MATH_DEFINES GLM_ENABLE_EXPERIMENTAL)
AR			:= llvm-ar
ARFLAGS		:= rc
BIN			:= bin
LIB			:= libqubit.a
INC			:= include
LD			:= lib
LDFLAGS		:= $(subst lib,,$(basename $(notdir $(addprefix -l ,$(wildcard $(LD)/*.a) m gdi32 user32 kernel32 pthread dl)))) 
SRC			:= src
SRCS 		:= $(wildcard $(SRC)/*.cpp $(SRC)/opengl/*.cpp)
OBJ			:= obj
OBJS		:= $(addprefix $(OBJ)/,$(notdir $(patsubst $(SRC)/%.cpp,$(SRC)/%.o,$(SRCS))))
TEST		:= test
TESTS		:= $(filter-out $(TEST)/main.cpp,$(wildcard $(TEST)/*.cpp))
RMDIR		:= rmdir /s /q
MKDIR		:= mkdir
ECHO		:= echo

.PHONY: all clean debug debug-config release release-config run gdb

all: debug

debug: OPTIMIZATION += -g -fno-math-errno

release: OPTIMIZATION += -Ofast -Werror

debug-build: $(BIN)/$(LIB) | $(TEST)\$(BIN)
	$(CPP) $(OPTIMIZATION) $(CPPFLAGS) $(MACROS) $(TEST)/main.cpp -I $(INC) -L $(BIN)/ -l $(subst lib,,$(basename $(LIB))) -L $(LD)/ $(LDFLAGS) -o $(TEST)/$(BIN)/main.exe

release-build: clean $(BIN)/$(LIB)

debug: debug-build
release: release-build

$(BIN)/$(LIB): $(OBJS) | $(BIN)
	$(AR) $(ARFLAGS) $(BIN)/$(LIB) $(OBJS)

$(OBJ)/%.o: $(SRC)/%.cpp | $(OBJ)
	$(CPP) $(OPTIMIZATION) $(CPPFLAGS) $(MACROS) -I $(INC)/ -c $< -o $@

$(OBJ)/%.o: $(SRC)/opengl/%.cpp | $(OBJ)
	$(CPP) $(OPTIMIZATION) $(CPPFLAGS) $(MACROS) -I $(INC)/ -c $< -o $@

$(BIN) $(OBJ) $(TEST)\$(BIN):
	$(shell $(MKDIR) $@)

run:
	./$(TEST)/$(BIN)/main.exe

gdb:
	gdb ./$(TEST)/$(BIN)/main.exe

clean:
	$(shell $(RMDIR) $(OBJ))
	$(shell $(RMDIR) $(TEST)\$(BIN))