#pragma once

#include <array>
#include <cstring>
#include <initializer_list>

#include <glad/gl.h>

namespace qb
{
    namespace opengl
    {
        using element_t = GLushort;

        template <const GLsizei element_count>
        struct element
        {
            static constexpr GLsizei size() { return element_count; }
            
            std::array<element_t, element_count> elements;

            element() = delete;

            constexpr element(const std::initializer_list<element_t> list)
            {
                if (list.size() != element_count) {
                    LOG_F(ERROR, "List does not match element size.");
                    std::exit(-1);
                }

                std::copy(list.begin(), list.end(), elements.begin());
            }

            constexpr element(const std::array<element_t, element_count> elements)
                :   elements(elements)
            { }

            ~element() = default;

            // TODO: Implement subscript operator?
            //
            // element_t& operator[] (GLuint index);
        };
    }
}