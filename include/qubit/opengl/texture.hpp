#pragma once

#include <string>

#include <glad/gl.h>
#include <glm/vec2.hpp>

namespace qb
{
    namespace opengl
    {
        class texture
        {
            private:
                GLuint _texture;
                glm::ivec2 _size;
                const std::string _path;

            public:
                texture(const std::string& path);

                texture() = delete;
                texture(const texture&) = delete;

                ~texture();

                const glm::ivec2
                get_size() const;

                const GLuint
                get_gl_object() const;

                void
                set_texture_param(const GLenum param, const GLint value) const;
        };
    }
}