#pragma once

#include <vector>
#include <string>

#include <glad/gl.h>

#include <qubit/opengl/vao.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/shader.hpp>

namespace qb
{
    namespace opengl
    {
        class instanced_renderer
        {
            private:
                const std::string _shader_alias;

                vao _vao;
                vbo _vbo;
                ebo _ebo;

                inline void
                begin() const;

                inline void
                end() const;
            
            public:
                instanced_renderer() = delete;
                
                instanced_renderer(const std::string& shader_alias, std::vector<vertex_buffer_layout> layouts);

                ~instanced_renderer() = default;

                vao&
                get_vao();

                vbo&
                get_vbo();

                ebo&
                get_ebo();

                shader&
                get_shader();

                void
                submit_vbo_data(const std::vector<float>& data);

                void
                submit_vbo_data(const float* data, const GLuint count);

                void
                submit_vbo_data(const GLuint offset, const float* data, const GLuint count);

                void
                submit_ebo_data(const std::vector<GLushort>& data);

                void
                submit_ebo_data(const GLushort* data, const GLuint count);

                void
                submit_ebo_data(const GLuint offset, const GLushort* data, const GLuint count);

                void
                draw(const GLenum mode = GL_TRIANGLES);
        };
    }
}