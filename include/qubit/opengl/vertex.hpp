#pragma once

#include <array>
#include <initializer_list>

#include <glad/gl.h>
#include <glm/glm.hpp>

namespace qb
{
    namespace opengl
    {
        struct __attribute__((packed)) vertex
        {
            static constexpr GLsizei size() { return 13; }
            
            glm::fvec3 position;
            glm::fvec4 color;
            glm::fvec2 texture_coords;
            float texture_index;
            glm::fvec3 normal;
        };
    }
}