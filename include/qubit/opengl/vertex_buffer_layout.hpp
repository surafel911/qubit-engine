#pragma once

#include <string>

#include <glad/gl.h>

#include <qubit/opengl/vertex_data_type.hpp>

namespace qb
{
    namespace opengl
    {
        struct vertex_buffer_layout
        {
            std::string name;
            vertex_data_type type;
            GLboolean normalize;
            GLuint divisor;

            vertex_buffer_layout(const std::string& name, const vertex_data_type type, const GLboolean normalize);
            vertex_buffer_layout(const std::string& name, const vertex_data_type type, const GLboolean normalize, const GLuint divisor);
        };
    }
}