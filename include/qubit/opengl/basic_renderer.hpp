#pragma once

#include <vector>
#include <string>

#include <glad/gl.h>

#include <qubit/opengl/vao.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/shader.hpp>

namespace qb
{
    namespace opengl
    {
        class basic_renderer
        {
            private:
                const std::string _shader_alias;

                vao _vao;
                vbo _vbo;

                GLuint _stride;

                inline void
                begin() const;

                inline void
                end() const;

            public:
                basic_renderer() = delete;
                
                basic_renderer(const std::string& shader_alias, std::vector<vertex_buffer_layout> layouts);

                ~basic_renderer() = default;

                vao&
                get_vao();

                vbo&
                get_vbo();

                ebo&
                get_ebo();

                shader&
                get_shader();

                void
                submit_data(const std::vector<float>& data);

                void
                submit_data(const float* data, const GLuint count);

                void
                submit_data(const GLuint offset, const float* data, const GLuint count);

                void
                draw(GLenum mode = GL_TRIANGLES);
        };
    }
}