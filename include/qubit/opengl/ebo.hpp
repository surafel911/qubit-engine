#pragma once

#include <vector>

#include <glad/gl.h>

namespace qb
{
    namespace opengl
    {
        class ebo
        {
            private:
                static const GLuint
                size(const GLuint count);
                
                GLenum _usage;
                GLuint _ebo, _count;

            public:
                ebo();
                ebo(ebo&& ebo);
                ebo(const GLenum usage, const std::vector<GLushort>& data);
                ebo(const GLenum usage, const GLuint count, const GLushort* data = nullptr);
                
                ebo(const ebo& ebo) = delete;

                ~ebo();

                const GLuint
                get_count() const;
                
                const GLuint
                get_gl_object() const;

                void
                submit_data(const GLuint offset, const GLuint count, const GLushort* data);

                ebo& operator =(ebo& other) = delete;
                const ebo& operator =(const ebo& other) = delete;
        };
    }
}