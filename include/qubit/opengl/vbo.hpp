#pragma once

#include <vector>

#include <glad/gl.h>

namespace qb
{   
    namespace opengl
    {
        class vbo
        {
            private:
                static const GLuint
                size(const GLuint count);

                GLenum _usage;
                GLuint _vbo, _count;
                
            public:
                vbo();
                vbo(vbo&& vbo);
                vbo(const GLenum usage, const std::vector<float>& data);
                vbo(const GLenum usage, const GLuint count, const float* data = nullptr);
                
                vbo(const vbo& vbo) = delete;

                ~vbo();

                const GLuint
                get_count() const;

                const GLuint
                get_gl_object() const;

                void
                submit_data(const GLuint offset, const GLuint count, const float* data);

                
                vbo& operator =(vbo& other) = delete;
                const vbo& operator =(const vbo& other) = delete;
        };
    }
}