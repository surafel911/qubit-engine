#pragma once

#include <string>
#include <unordered_map>

#include <qubit/opengl/shader.hpp>

namespace qb
{
    class engine;

    namespace opengl
    {
        // TODO: Abstract shader class to support multiple APIs
        class shader_manager
        {
            friend class qb::engine;

            private:
                std::unordered_map<std::string, shader> _shader_map;

                shader_manager() = default;
                ~shader_manager() = default;

            public:
                static const std::string
                make_alias(const std::pair<const std::string, const std::string>& paths);

                static const std::string
                make_alias(const std::string& vertex_path, const std::string& fragment_path);
                
                shader&
                create_shader(
                    const std::string& alias,
                    const std::pair<const std::string, const std::string>& paths
                );

                shader&
                create_shader(
                    const std::string& alias,
                    const std::string& vertex_path,
                    const std::string& fragment_path
                );

                void
                destroy_shader(const std::string& alias);

                shader&
                find_shader(const std::string& alias);
        };
    }
}