#pragma once

#include <string>
#include <vector>
#include <unordered_map>

#include <glad/gl.h>
#include <glm/glm.hpp>

#include <qubit/opengl/texture.hpp>

namespace qb
{
    namespace opengl
    {
        class shader_manager;

        class shader
        {
            friend class shader_manager;
            
            private:
                GLuint _vertex, _fragment, _program;

                std::unordered_map<std::string, GLint> _location_cache;

                const GLuint
                create_shader(const std::string& path, const GLenum type);

                const GLint
                find_attrib_location_from_cache(const std::string& name);

                const GLint
                find_uniform_location_from_cache(const std::string& name);


            public:
                shader(const std::pair<const std::string, const std::string>& paths);
                shader(const std::string& vertex_path, const std::string& fragment_path);

                static void
                bind(const shader& shader);

                static void
                unbind();
                
                ~shader();

                shader() = delete;
                shader(const shader&) = delete;

                const GLint
                get_attrib_location(const std::string& name);

                const GLint
                get_uniform_location(const std::string& name);

                void
                set_sampler_unit(const std::string& name, const std::string& texture, const GLuint unit);

                void
                set_uniform_data(const std::string& name, const float data);

                void
                set_uniform_data(const std::string& name, const glm::fvec1& data);

                void
                set_uniform_data(const std::string& name, const glm::fvec2& data);
                
                void
                set_uniform_data(const std::string& name, const glm::fvec3& data);

                void
                set_uniform_data(const std::string& name, const glm::fvec4& data);

                void
                set_uniform_data(const std::string& name, const glm::fmat3& data, const GLboolean transpose = GL_FALSE);

                void
                set_uniform_data(const std::string& name, const glm::fmat4& data, const GLboolean transpose = GL_FALSE);

                void
                set_uniform_data(const std::string& name, const std::vector<glm::fvec1>& data);

                void
                set_uniform_data(const std::string& name, const std::vector<glm::fvec2>& data);
                
                void
                set_uniform_data(const std::string& name, const std::vector<glm::fvec3>& data);

                void
                set_uniform_data(const std::string& name, const std::vector<glm::fvec4>& data);

                void
                set_uniform_data(const std::string& name, const std::vector<glm::fmat3>& data, const GLboolean transpose = GL_FALSE);

                void
                set_uniform_data(const std::string& name, const std::vector<glm::fmat4>& data, const GLboolean transpose = GL_FALSE);
        };
    }
}