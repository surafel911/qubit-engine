#pragma once

#include <string>
#include <unordered_map>

#include <qubit/opengl/texture.hpp>

namespace qb
{
    namespace opengl
    {
        class texture_manager
        {
            private:
                std::unordered_map<std::string, texture> _texture_map;

            public:
                texture&
                create_texture(const std::string& path);

                texture&
                create_texture(const std::string& alias, const std::string& path);

                void
                destroy_texture(const std::string& name);
                
                texture&
                find_texture(const std::string& name);
        };
    }
}