#pragma once

#include <array>
#include <string>
#include <vector>
#include <initializer_list>

#include <glad/gl.h>
#include <glm/glm.hpp>

#include <qubit/opengl/ebo.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/shader.hpp>
#include <qubit/opengl/vertex_data_type.hpp>
#include <qubit/opengl/vertex_buffer_layout.hpp>

namespace qb
{
    namespace opengl
    {
        class vao
        {
            private:
                GLuint _vao;

                const int
                calc_vertex_buffer_stride(
                    const std::vector<vertex_buffer_layout>& layouts
                );

                void
                set_vertex_vector_layout(
                    shader& shader,
                    const vbo& vbo,
                    const vertex_buffer_layout& layout,
                    const GLuint offset,
                    const GLuint stride,
                    const GLuint binding_index
                );

                void
                set_vertex_matrix_layout(
                    shader& shader,
                    const vbo& vbo,
                    const vertex_buffer_layout& layout,
                    const GLuint offset,
                    const GLuint stride,
                    const GLuint binding_index
                );

                const GLuint
                set_vertex_buffer_layouts(
                    shader& shader,
                    const vbo& vbo,
                    const std::vector<vertex_buffer_layout>& layouts
                );

            public:
                static void
                bind(const vao& vao);

                static void
                unbind();

                vao();
                vao(vao&& vao);

                vao(const vao& vao) = delete;

                ~vao();

                const GLuint
                submit_vbo(
                    const vbo& vbo,
                    shader& shader,
                    const std::vector<vertex_buffer_layout>& layouts
                );

                void
                submit_ebo(const ebo& ebo);

                const GLuint
                get_gl_object() const;

                vao& operator =(vao& other) = delete;
                const vao& operator =(const vao& other) = delete;
        };
    }
}