#pragma once

#include <glm/glm.hpp>

namespace qb
{
    namespace opengl
    {
        enum class vertex_data_type
        {
            float1 = glm::fvec1::length(),
            float2 = glm::fvec2::length(),
            float3 = glm::fvec3::length(),
            float4 = glm::fvec4::length(),
            mat3 = glm::fmat3::length() * glm::fmat3::length(),
            mat4 = glm::fmat4::length() * glm::fmat4::length(),
        };
    }
}