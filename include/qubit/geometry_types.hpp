#pragma once

#include <qubit/geometry.hpp>
#include <qubit/geometry_manager.hpp>

// TODO: Ensure a CW or CCW orientation for culling
namespace qb
{
    struct plane_geometry
    {
        public:
            static constexpr unsigned int plane_vertex_count = 4;
            static constexpr unsigned int plane_element_count = 6;

        private:
            static constexpr std::array<qb::opengl::vertex, plane_vertex_count> vertices_identity = {{
                    //     COORDINATES          /        COLORS                 /   TexCoord        / TexIndex      /   NORMALS     //
                    {{-0.5f, 0.0f,  0.5f},      {0.0f, 0.0f, 0.0f, 0.0f},	    {0.0f, 0.0f},       0.0f,         {0.0f, 1.0f, 0.0f}},
                    {{-0.5f, 0.0f, -0.5f},      {0.0f, 0.0f, 0.0f, 0.0f},	    {0.0f, 1.0f},       0.0f,         {0.0f, 1.0f, 0.0f}},
                    {{ 0.5f, 0.0f, -0.5f},      {0.0f, 0.0f, 0.0f, 0.0f},	    {1.0f, 1.0f},       0.0f,         {0.0f, 1.0f, 0.0f}},
                    {{ 0.5f, 0.0f,  0.5f},      {0.0f, 0.0f, 0.0f, 0.0f},	    {1.0f, 0.0f},       0.0f,         {0.0f, 1.0f, 0.0f}},
            }};

            static constexpr std::array<GLushort, plane_element_count> elements_identity = {
                0, 1, 2,
                0, 2, 3,
            };

        public:
            static constexpr qb::geometry<plane_vertex_count> plane_geometry_identity {plane_geometry::vertices_identity};
            static constexpr qb::opengl::element<plane_element_count> plane_elements_identity {plane_geometry::elements_identity};

        plane_geometry() = delete;
    };

    
    using plane = geometry<plane_geometry::plane_vertex_count>;

    class plane_manager : public geometry_manager<plane_geometry::plane_vertex_count, plane_geometry::plane_element_count>
    {
        public:
            plane_manager()
                :   geometry_manager(plane_geometry::plane_geometry_identity, plane_geometry::plane_elements_identity)
            { }
    };

    class cube_geometry
    {
        public:
            static constexpr unsigned int cube_vertex_count = 8;
            static constexpr unsigned int cube_element_count = 36;

        private:
            static constexpr std::array<qb::opengl::vertex, cube_vertex_count> vertices_identity = {{
                //     COORDINATES          /        COLORS                 /   TexCoord        / TexIndex      /   NORMALS     //
                {{-0.5f, -0.5f,  0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{0.0f, 0.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
                {{-0.5f, -0.5f, -0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{0.0f, 1.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
                {{ 0.5f, -0.5f, -0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{1.0f, 1.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
                {{ 0.5f, -0.5f,  0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{1.0f, 0.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
                {{-0.5f,  0.5f,  0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{0.0f, 0.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
                {{-0.5f,  0.5f, -0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{0.0f, 1.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
                {{ 0.5f,  0.5f, -0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{1.0f, 1.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
                {{ 0.5f,  0.5f,  0.5f},		{1.0f, 1.0f, 1.0f, 1.0f},		{1.0f, 0.0f},       0.0f,           {0.0f, 1.0f, 0.0f}},
            }};

            static constexpr std::array<GLushort, cube_element_count> elements_identity = {
                0, 1, 2,
                0, 2, 3,
                0, 4, 7,
                0, 7, 3,
                3, 7, 6,
                3, 6, 2,
                2, 6, 5,
                2, 5, 1,
                1, 5, 4,
                1, 4, 0,
                4, 5, 6,
                4, 6, 7,
            };

        public:
            static constexpr qb::geometry<cube_vertex_count> cube_geometry_identity {cube_geometry::vertices_identity};
            static constexpr qb::opengl::element<cube_element_count> cube_elements_identity {cube_geometry::elements_identity};

        cube_geometry() = delete;
    };

    using cube = geometry<cube_geometry::cube_vertex_count>;

    class cube_manager : public geometry_manager<cube_geometry::cube_vertex_count, cube_geometry::cube_element_count>
    {
        public:
            cube_manager()
                :   geometry_manager(cube_geometry::cube_geometry_identity, cube_geometry::cube_elements_identity)
            { }
    };

    class pyramid_geometry
    {
        public:
            static constexpr unsigned int pyramid_vertex_count = 16;
            static constexpr unsigned int pyramid_element_count = 18;

        private:
            static constexpr std::array<qb::opengl::vertex, pyramid_vertex_count> vertices_identity = {{
                //     COORDINATES          /        COLORS                 /    TexCoord       /   TexIndex    /        NORMALS       //
                {{-0.5f, 0.0f,  0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {0.0f, 0.0f},       0.0f,           { 0.0f, -1.0f, 0.0f}}, // Bottom side
                {{-0.5f, 0.0f, -0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {0.0f, 5.0f},       0.0f,           { 0.0f, -1.0f, 0.0f}}, // Bottom side
                {{ 0.5f, 0.0f, -0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {5.0f, 5.0f},       0.0f,           { 0.0f, -1.0f, 0.0f}}, // Bottom side
                {{ 0.5f, 0.0f,  0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {5.0f, 0.0f},       0.0f,           { 0.0f, -1.0f, 0.0f}}, // Bottom side
                {{-0.5f, 0.0f,  0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {0.0f, 0.0f},       0.0f,           {-0.8f, 0.5f,  0.0f}}, // Left Side
                {{-0.5f, 0.0f, -0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {5.0f, 0.0f},       0.0f,           {-0.8f, 0.5f,  0.0f}}, // Left Side
                {{ 0.0f, 0.8f,  0.0f},      {1.0f, 1.0f, 1.0f, 1.0f},       {2.5f, 5.0f},       0.0f,           {-0.8f, 0.5f,  0.0f}}, // Left Side
                {{-0.5f, 0.0f, -0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {5.0f, 0.0f},       0.0f,           { 0.0f, 0.5f, -0.8f}}, // Non-facing side
                {{ 0.5f, 0.0f, -0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {0.0f, 0.0f},       0.0f,           { 0.0f, 0.5f, -0.8f}}, // Non-facing side
                {{ 0.0f, 0.8f,  0.0f},      {1.0f, 1.0f, 1.0f, 1.0f},       {2.5f, 5.0f},       0.0f,           { 0.0f, 0.5f, -0.8f}}, // Non-facing side
                {{ 0.5f, 0.0f, -0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {0.0f, 0.0f},       0.0f,           { 0.8f, 0.5f,  0.0f}}, // Right side
                {{ 0.5f, 0.0f,  0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {5.0f, 0.0f},       0.0f,           { 0.8f, 0.5f,  0.0f}}, // Right side
                {{ 0.0f, 0.8f,  0.0f},      {1.0f, 1.0f, 1.0f, 1.0f},       {2.5f, 5.0f},       0.0f,           { 0.8f, 0.5f,  0.0f}}, // Right side
                {{ 0.5f, 0.0f,  0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {5.0f, 0.0f},       0.0f,           { 0.0f, 0.5f,  0.8f}}, // Facing side
                {{-0.5f, 0.0f,  0.5f},      {1.0f, 1.0f, 1.0f, 1.0f},       {0.0f, 0.0f},       0.0f,           { 0.0f, 0.5f,  0.8f}}, // Facing side
                {{ 0.0f, 0.8f,  0.0f},      {1.0f, 1.0f, 1.0f, 1.0f},       {2.5f, 5.0f},       0.0f,           { 0.0f, 0.5f,  0.8f}},  // Facing side
            }};

            // Indices for vertices order
            static constexpr std::array<GLushort, pyramid_element_count> elements_identity = {
                0, 1, 2, // Bottom side
                0, 2, 3, // Bottom side
                4, 6, 5, // Left side
                7, 9, 8, // Non-facing side
                10, 12, 11, // Right side
                13, 15, 14 // Facing side
            };

        public:
            static constexpr qb::geometry<pyramid_vertex_count> pyramid_geometry_identity {pyramid_geometry::vertices_identity};
            static constexpr qb::opengl::element<pyramid_element_count> pyramid_elements_identity {pyramid_geometry::elements_identity};

            pyramid_geometry() = delete;
    };

    using pyramid = geometry<pyramid_geometry::pyramid_vertex_count>;
    
    class pyramid_manager : public geometry_manager<pyramid_geometry::pyramid_vertex_count, pyramid_geometry::pyramid_element_count>
    {
        public:
            pyramid_manager()
                :   geometry_manager(pyramid_geometry::pyramid_geometry_identity, pyramid_geometry::pyramid_elements_identity)
            { }
    };
}