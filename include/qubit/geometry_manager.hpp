#pragma once

#include <array>
#include <vector>
#include <algorithm>
#include <initializer_list>

#include <glad/gl.h>
#include <glm/glm.hpp>

#include <qubit/geometry.hpp>
#include <qubit/opengl/vertex.hpp>
#include <qubit/opengl/element.hpp>

    /*
    * How to get position given transformation matrix (assuming glm::mat4):
    * 
    * 1. glm::vec3(m[3])
    * 
    * 2. glm::vec3(m * glm::vec3(1))
    * 
    * 3. GLM supports matrix decomposition
    *      #include <glm/gtx/matrix_decompose.hpp>
    *      
    *      glm::mat4 transformation; // your transformation matrix.
    *      glm::vec3 scale;
    *      glm::quat rotation;
    *      glm::vec3 translation;
    *      glm::vec3 skew;
    *      glm::vec4 perspective;
    *      glm::decompose(transformation, scale, rotation, translation, skew, perspective);
    */

namespace qb
{
    
    using id_t = GLshort;

    template <const GLsizei vertex_count, const GLsizei element_count>
    class geometry_manager
    {
        private:
            id_t _id_index;
            bool _changed;
            opengl::element_t _element_stride;
            geometry<vertex_count> _geometry_identity;
            qb::opengl::element<element_count> _element_pattern;

            // TODO: Consider reimplementing geometry to SoA instead of an AoS
            std::vector<id_t> _ids;
            std::vector<geometry<vertex_count>> _geometry;
            std::vector<glm::fmat4> _transforms;
            std::vector<opengl::element<element_count>> _elements;
            
            constexpr const int
            find_index(const id_t id)
            {
                return (std::find(this->_ids.begin(), this->_ids.end(), id) -
                    this->_ids.begin());
            }

            constexpr const int
            find_element_stride(opengl::element<element_count> element)
            {
                int i = 0, max = element.elements[0];

                for (i = 1; i < element.elements.size(); i++) {
                    if (element.elements[i] > max) {
                        max = element.elements[i];
                    }
                }
            
                return max + 1;
            }

            constexpr const opengl::element<element_count>
            generate_element(const id_t index)
            {
                opengl::element<element_count> element = this->_element_pattern;

                for (int i = 0; i < element_count; ++i) {
                    element.elements[i] += _element_stride * index;
                }

                return element;
            }

        public:
            static constexpr GLsizei get_vertex_count() { return vertex_count; }

            static constexpr GLsizei get_element_count() { return element_count; }

            geometry_manager() = delete;

            geometry_manager(
                const geometry<vertex_count>& geometry_identity,
                const opengl::element<element_count>& element_pattern
            )   :   _id_index(0),
                    _changed(false),
                    _element_stride(find_element_stride(element_pattern)),
                    _geometry_identity(geometry_identity),
                    _element_pattern(element_pattern)
            {
                static_assert(element_count % 3 == 0, "Element count needs to be divisible by 3");
            }

            ~geometry_manager() = default;

            const std::pair<const float*, const GLsizei>
            get_vertices()
            {
                return std::pair<const float*, const GLsizei>((float*)this->_geometry.data(), vertex_count * opengl::vertex::size() * this->_geometry.size());
            }

            const std::pair<const float*, const GLsizei>
            get_transforms()
            {
                return std::pair<const float*, const GLsizei>((float*)this->_transforms.data(), glm::fmat4::length() * this->transforms.size());
            }

            const std::pair<const opengl::element_t*, const GLsizei>
            get_elements()
            {
                if (this->_changed) {
                    this->_elements.clear();

                    for (int i = 0; i < this->_geometry.size(); ++i) {
                        this->_elements.push_back(this->generate_element(i));
                    }

                    this->_changed = false;
                }

                return std::pair<const opengl::element_t*, const GLsizei>((opengl::element_t*)this->_elements.data(), element_count * this->_elements.size());
            }

            const id_t
            create_geometry()
            {
                return this->create_geometry(this->_geometry_identity);
            }

            const id_t
            create_geometry(const geometry<vertex_count>& value)
            {
                this->_ids.emplace_back(++_id_index);
                this->_geometry.emplace_back(value);
                this->_transforms.emplace_back(1.0f);

                this->_changed = true;

                return this->_ids.back();
            }

            const id_t
            create_geometry(const std::array<opengl::vertex, vertex_count>& value)
            {
                return this->create_geometry(geometry<vertex_count>(value));
            }

            const id_t
            create_geometry(std::initializer_list<float>& list)
            {
                return this->create_geometry(geometry<vertex_count>(list));
            }

            void
            destroy_geometry(id_t& id)
            {
                const int index = this->find_index(id);

                this->_ids.erase(this->_ids.begin() + index);
                this->_geometry.erase(this->_geometry.begin() + index);
                this->_transforms.erase(this->_transforms.begin() + index);

                this->_changed = true;
            }

            geometry<vertex_count>&
            find_geometry(const id_t id)
            {
                return this->_geometry[this->find_index(id)];
            }

            glm::fmat4&
            find_transform(const id_t id)
            {
                return this->_transforms[this->find_index(id)];
            }

            const std::size_t
            get_size() const
            {
                return this->_ids.size();
            }
    };
}