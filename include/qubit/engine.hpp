#pragma once

#include <GLFW/glfw3.h>

#include <qubit/glfw_context.hpp>
#include <qubit/loguru_context.hpp>
#include <qubit/window.hpp>

// TODO: Avoid having GL classes in main engine
#include <qubit/opengl/shader_manager.hpp>
#include <qubit/opengl/texture_manager.hpp>


namespace qb
{
    class engine
    {
        public:
            struct config
            {
                int argc;
                loguru::NamedVerbosity verbosity;
                char** argv;
                window::config window_config;

                config() = default;
                config(const loguru::NamedVerbosity verbosity, int argc, char** argv, const window::config& window_config);
            };

        private:
            engine(config& config);

            float _time_step;

            loguru_context _loguru_context;
            glfw_context _glfw_context;
            window _window;
            opengl::shader_manager _shader_manager;
            opengl::texture_manager _texture_manager;

            void
            calc_time_step();
            
        public:
            ~engine();

            static void
            create(const config& config);

            static engine*
            get(const config& config);

            static engine*
            get();

            window&
            get_window();

            opengl::shader_manager&
            get_shader_manager();

            
            opengl::texture_manager&
            get_texture_manager();

            const float
            get_time_step() const;


            void
            update();
    };
}