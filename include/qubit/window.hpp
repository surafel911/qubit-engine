#pragma once

#include <string>

#include <glm/vec2.hpp>
#include <GLFW/glfw3.h>

namespace qb
{
    class engine;

    class window
    {
        friend class engine;

        public:
            struct config
            {
                std::string title;
                glm::ivec2 size;
                bool fullscreen;

                config() = default;
                config(const std::string& title, const glm::ivec2& size, const bool fullscreen);
            };

        private:
            window(const config& config);
            ~window();

            GLFWwindow* _window;
        public:
            bool
            should_close() const;

            GLFWwindow*
            get_handle() const;

            const glm::ivec2
            get_size() const;

            const float
            get_aspect() const;
    };
}