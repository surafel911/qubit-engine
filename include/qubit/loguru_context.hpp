#pragma once

#include <loguru/loguru.hpp>

namespace qb
{
    class loguru_context
    {
        public:
            loguru_context() = delete;
            loguru_context(const loguru::NamedVerbosity verbosity, int argc, char** argv);
    };
}