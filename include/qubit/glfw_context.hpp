#pragma once

namespace qb
{
    class engine;

    class glfw_context
    {
        friend class qb::engine;
        
        private:
            glfw_context();
            ~glfw_context();
    };
}