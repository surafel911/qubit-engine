#pragma once

#include <array>
#include <initializer_list>

#include <glad/gl.h>

#include <qubit/opengl/vertex.hpp>

namespace qb
{
    // TODO: Consider reimplementing geometry to SoA instead of an AoS
    template <const GLsizei vertex_count>
    class geometry
    {
        private:
            std::array<opengl::vertex, vertex_count> _vertices;

        public:
            static constexpr GLsizei get_vertex_count() { return vertex_count; }

            geometry() = default;

            constexpr geometry(const std::initializer_list<float>& list)
                :   _vertices(list)
            { }

            constexpr geometry(const std::array<opengl::vertex, vertex_count>& vertices)
                :   _vertices(vertices)
            { }

            constexpr geometry(const std::array<float, vertex_count * opengl::vertex::size()>& vertices)
            {
                std::copy((float*)vertices.begin(), (float*)vertices.end(), (float*)this->_vertices.begin());
            }

            ~geometry() = default;

            std::array<opengl::vertex, vertex_count>&
            get_vertices()
            {
                return this->_vertices;
            }

            // TODO: Implement subscript operator?
            //
            // GLushort& operator[] (GLuint index);
    };
}