#pragma once

#include <array>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace qb
{
    template <typename glm::fmat4 projection(float,float,float,float)>
    class camera
    {
        protected:
            glm::fvec3 _up, _position, _orientation;

        public:
            camera() = default;
            camera(const glm::fvec3& position)
                :   _up(0.0f, 1.0f, 0.0f), _position(position),
                    _orientation(0.0f, 0.0f, -1.0f)
            { }
        
            glm::fvec3
            get_up() const
            {
                return this->_up;
            }

            glm::fvec3
            get_position() const
            {
                return this->_position;
            }

            glm::fvec3
            get_orientation() const
            {
                return this->_orientation;
            }
            
            void
            set_up(const glm::fvec3& up)
            {
                this->_up = up;
            }

            void
            set_position(const glm::fvec3& position)
            {
                this->_position = position;
            }

            void
            set_orientation(const glm::fvec3& orientation)
            {
                this->_orientation = orientation;
            }

            glm::fvec3
            translate(const glm::fvec3& translate)
            {
                this->_position += translate;
                return this->_position;
            }

            std::array<glm::fmat4, 2>
            operator()(const float fov, const float aspect, const float near, const float far) const
            {
                return this->get_matrix(fov, aspect, near, far);
            }

            glm::fmat4
            get_matrix(const float fov, const float aspect, const float near, const float far) const
            {
                std::array<glm::fmat4, 2> matrices = this->get_matrices();

                return (matrices[1] * matrices[0]);
            }

            std::array<glm::fmat4, 2>
            get_matrices(const float fov, const float aspect, const float near, const float far) const
            {
                glm::fmat4 view(1.0f), proj(1.0f);

                view = glm::lookAt(this->_position, this->_position + this->_orientation, this->_up);
                proj = projection(glm::radians(fov), aspect, near, far);

                return {view, proj};
            }
    };

    using orthographic_camera = camera<glm::ortho>;
    using perpsective_camera = camera<glm::perspective>;

    class controlable_camera : public camera<glm::perspective>
    {
        protected:
            static float default_speed;
            static float default_sensitivity;

            bool _first_click;
            float _speed, _sensitivity;

        public:
            static const float
            get_default_speed();

            static void
            set_default_speed(const float speed);

            static const float
            get_default_sensitivity();

            static void
            set_default_sensitivity(const float sensitivity);

            controlable_camera()
                :   camera(), _first_click(true),
                    _speed(default_speed), _sensitivity(default_sensitivity)
            { }

            controlable_camera(const glm::fvec3& position)
                :   camera(position), _first_click(true),
                    _speed(1.5f), _sensitivity(20.f)
            { }
            
            controlable_camera(const float speed, const float sensitivity)
                :   camera(), _first_click(true),
                    _speed(speed), _sensitivity(sensitivity)
            { }

            controlable_camera(const glm::fvec3& position, const float speed, const float sensitivity)
                :   camera(position), _first_click(true),
                    _speed(speed), _sensitivity(sensitivity)
            { }


            virtual void
            input(float time_step) = NULL;
    };

    class first_person_camera : public controlable_camera
    {
        public:
            virtual void
            input(float time_step) override;
    };

    class third_person_camera :  public controlable_camera
    { 
        public:
            third_person_camera()
                :   controlable_camera()
            { }

            third_person_camera(const glm::fvec3& position)
                :   controlable_camera(position)
            { }

            third_person_camera(const glm::fvec3& position, const float speed, const float sensitivity)
                :   controlable_camera(position, speed, sensitivity)
            { }

            virtual void
            input(float time_step) override;
    };
}