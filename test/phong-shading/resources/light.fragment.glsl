#version 460 compatibility

out vec4 fcol;

uniform vec4 light_col;

void
main()
{
    fcol = light_col;
}
