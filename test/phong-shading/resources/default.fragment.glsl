#version 460 compatibility

in vec4 vcol;

out vec4 fcol;

void
main()
{
    fcol = vcol;
}