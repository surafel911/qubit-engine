#version 460 compatibility

in vec3 pos;
// in vec4 col;
in vec2 texcoord;
in vec3 normal;

// out vec4 vcol;
out vec2 vtexcoord;
out vec3 vnormal;
out vec3 vposition;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

uniform mat4 normal_matrix;

void
main()
{
    // vcol = col;
    vtexcoord = texcoord;
    vnormal = normal;

    gl_PointSize = 25.0;

    vnormal = vec3(normal_matrix * vec4(normal, 1.0f));
    vposition = vec3(model * vec4(pos, 1.0f));
    gl_Position = proj * view * vec4(vposition, 1.0f);
}