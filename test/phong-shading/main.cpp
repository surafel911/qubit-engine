#include <cmath>
#include <string>
#include <vector>
#include <iostream>

#include <qubit/quad.hpp>
#include <qubit/engine.hpp>
#include <qubit/camera.hpp>

#include <qubit/opengl/ebo.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/vao.hpp>
#include <qubit/opengl/index.hpp>
#include <qubit/opengl/vertex.hpp>
#include <qubit/opengl/shader.hpp>
#include <qubit/opengl/texture.hpp>
#include <qubit/opengl/basic_renderer.hpp>
#include <qubit/opengl/element_renderer.hpp>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <loguru/loguru.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct pyramid_geometry
{
    glm::fmat4 model;

    // Face normals (as apposed to vertex normals) for flat shading (as apposed to smooth shading)
    std::array<GLfloat, 192> vertices = {
        //     COORDINATES     /        COLORS              /    TexCoord   /        NORMALS        //
        -0.5f, 0.0f,  0.5f,    0.83f, 0.70f, 0.44f, 1.0f,   0.0f, 0.0f,      0.0f, -1.0f, 0.0f,     // Bottom side
        -0.5f, 0.0f, -0.5f,    0.83f, 0.70f, 0.44f, 1.0f,	0.0f, 5.0f,      0.0f, -1.0f, 0.0f,     // Bottom side
        0.5f, 0.0f, -0.5f,     0.83f, 0.70f, 0.44f, 1.0f,	5.0f, 5.0f,      0.0f, -1.0f, 0.0f,     // Bottom side
        0.5f, 0.0f,  0.5f,     0.83f, 0.70f, 0.44f, 1.0f,	5.0f, 0.0f,      0.0f, -1.0f, 0.0f,     // Bottom side

        -0.5f, 0.0f,  0.5f,    0.83f, 0.70f, 0.44f, 1.0f, 	0.0f, 0.0f,     -0.8f, 0.5f,  0.0f,     // Left Side
        -0.5f, 0.0f, -0.5f,    0.83f, 0.70f, 0.44f, 1.0f,   5.0f, 0.0f,     -0.8f, 0.5f,  0.0f,     // Left Side
         0.0f, 0.8f,  0.0f,    0.92f, 0.86f, 0.76f, 1.0f,   2.5f, 5.0f,     -0.8f, 0.5f,  0.0f,     // Left Side

        -0.5f, 0.0f, -0.5f,    0.83f, 0.70f, 0.44f, 1.0f,	5.0f, 0.0f,      0.0f, 0.5f, -0.8f,     // Non-facing side
         0.5f, 0.0f, -0.5f,    0.83f, 0.70f, 0.44f, 1.0f,	0.0f, 0.0f,      0.0f, 0.5f, -0.8f,     // Non-facing side
         0.0f, 0.8f,  0.0f,    0.92f, 0.86f, 0.76f, 1.0f,	2.5f, 5.0f,      0.0f, 0.5f, -0.8f,     // Non-facing side

        0.5f, 0.0f, -0.5f,     0.83f, 0.70f, 0.44f, 1.0f,	0.0f, 0.0f,      0.8f, 0.5f,  0.0f,     // Right side
        0.5f, 0.0f,  0.5f,     0.83f, 0.70f, 0.44f, 1.0f,	5.0f, 0.0f,      0.8f, 0.5f,  0.0f,     // Right side
        0.0f, 0.8f,  0.0f,     0.92f, 0.86f, 0.76f, 1.0f,	2.5f, 5.0f,      0.8f, 0.5f,  0.0f,     // Right side

         0.5f, 0.0f,  0.5f,    0.83f, 0.70f, 0.44f, 1.0f,	5.0f, 0.0f,      0.0f, 0.5f,  0.8f,     // Facing side
        -0.5f, 0.0f,  0.5f,    0.83f, 0.70f, 0.44f, 1.0f, 	0.0f, 0.0f,      0.0f, 0.5f,  0.8f,     // Facing side
         0.0f, 0.8f,  0.0f,    0.92f, 0.86f, 0.76f, 1.0f,	2.5f, 5.0f,      0.0f, 0.5f,  0.8f,      // Facing side
    };

    std::array<GLushort, 18> elements = {
        0, 1, 2,    // Bottom side
        0, 2, 3,    // Bottom side

        4, 6, 5,    // Left side

        7, 9, 8,    // Non-facing side

        10, 12, 11, // Right side

        13, 15, 14  // Facing side
    };

    pyramid_geometry() : model(1.0f)
    { }
};

struct cube_geometry
{
    glm::fmat4 model;

    std::array<GLfloat, 24> vertices = {
         //     COORDINATES     //
        -0.1f, -0.1f,  0.1f,
        -0.1f, -0.1f, -0.1f,
         0.1f, -0.1f, -0.1f,
         0.1f, -0.1f,  0.1f,
        -0.1f,  0.1f,  0.1f,
        -0.1f,  0.1f, -0.1f,
         0.1f,  0.1f, -0.1f,
         0.1f,  0.1f,  0.1f
    };

    std::array<GLushort, 36> elements = {
        0, 1, 2,
        0, 2, 3,
        0, 4, 7,
        0, 7, 3,
        3, 7, 6,
        3, 6, 2,
        2, 6, 5,
        2, 5, 1,
        1, 5, 4,
        1, 4, 0,
        4, 5, 6,
        4, 6, 7
    };
    
    cube_geometry() : model(1.0f)
    { }
};

int
main(int argc, char* argv[])
{
    qb::engine* e = qb::engine::get(qb::engine::config(
        argc, argv,
        qb::window::config("Hello World", glm::ivec2(1280, 720), false)
    ));

    glEnable(GL_DEBUG_OUTPUT);

    glEnable(GL_DEPTH_TEST);
    // glDepthFunc(GL_LESS);
    // glFrontFace(GL_CW);
    // glCullFace(GL_BACK);
    // glEnable(GL_CULL_FACE);
    glEnable(GL_PROGRAM_POINT_SIZE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
    qb::opengl::shader_manager& shader_manager = e->get_shader_manager();

    shader_manager.create_shader("default", std::make_pair(
            "test/resources/default.vertex.glsl",
            "test/resources/default.fragment.glsl"
        )
    );

    shader_manager.create_shader("phong", std::make_pair(
            "test/resources/phong.vertex.glsl",
            "test/resources/phong.fragment.glsl"
        )
    );

    shader_manager.create_shader("light", std::make_pair(
            "test/resources/light.vertex.glsl",
            "test/resources/light.fragment.glsl"
        )
    );

    pyramid_geometry pyramid_geometry;
    qb::opengl::element_renderer pyramid_renderer("phong",
        {
            {"pos", qb::opengl::vertex_data_type::float3, GL_FALSE},
            {"", qb::opengl::vertex_data_type::float4, GL_FALSE},
            {"texcoord", qb::opengl::vertex_data_type::float2, GL_FALSE},
            {"normal", qb::opengl::vertex_data_type::float3, GL_FALSE},
        }
    );

    pyramid_renderer.submit_vbo_data(pyramid_geometry.vertices.size(), pyramid_geometry.vertices.data());
    pyramid_renderer.submit_ebo_data(pyramid_geometry.elements.size(), pyramid_geometry.elements.data());

    qb::opengl::texture texture("test/resources/brick.png");
    qb::opengl::texture::bind(texture, 0);

    cube_geometry cube_geometry;
    qb::opengl::element_renderer cube_renderer("light",
        {
            {"pos", qb::opengl::vertex_data_type::float3, GL_FALSE},
        }
    );

    cube_renderer.submit_vbo_data(cube_geometry.vertices.size(), cube_geometry.vertices.data());
    cube_renderer.submit_ebo_data(cube_geometry.elements.size(), cube_geometry.elements.data());

    cube_geometry.model = glm::translate(cube_geometry.model, {0.3f, 0.3f, 0.3f});  
    cube_geometry.model = glm::scale(cube_geometry.model, glm::fvec3(0.25f));

    qb::third_person_camera camera({0.0f, 0.2f, 20.0f});

    glm::vec3 light_pos = glm::vec3(cube_geometry.model * glm::fvec4(1.0f));
    glm::vec4 light_col = {1.0f, 1.0f, 1.0f, 1.0f};

    glClearColor(.07f, .13f, .17f, 1.0f);
    while (!e->get_window().should_close()) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        std::array<glm::fmat4, 2> matrices = camera.get_matrices(glm::radians(90.0f), e->get_window().get_aspect(), 0.1f, 100.0f);

        pyramid_geometry.model = glm::fmat4(1.0f);
#if 1
        static float rotation = 0.0f;  
        pyramid_geometry.model = glm::rotate(pyramid_geometry.model, glm::radians(rotation += 45.0f * e->get_time_step()), glm::fvec3(0.0f, 1.0f, 0.0f));
#endif
        pyramid_geometry.model = glm::scale(pyramid_geometry.model, glm::fvec3(0.25f));
        
        pyramid_renderer.get_shader().set_uniform_data("model", pyramid_geometry.model);
        pyramid_renderer.get_shader().set_uniform_data("view", matrices[0]);
        pyramid_renderer.get_shader().set_uniform_data("proj", matrices[1]);
        pyramid_renderer.get_shader().set_uniform_data("light_pos", light_pos);
        pyramid_renderer.get_shader().set_uniform_data("light_col", light_col);
        pyramid_renderer.get_shader().set_uniform_data("camera_pos", camera.get_position());
        pyramid_renderer.get_shader().set_uniform_data("normal_matrix", glm::inverse(pyramid_geometry.model), GL_TRUE);

        pyramid_renderer.draw();
        
        cube_renderer.get_shader().set_uniform_data("model", cube_geometry.model);
        cube_renderer.get_shader().set_uniform_data("view", matrices[0]);
        cube_renderer.get_shader().set_uniform_data("proj", matrices[1]);
        cube_renderer.get_shader().set_uniform_data("light_col", light_col);

        cube_renderer.draw();

        camera.input(e->get_time_step());

        e->update();
    }

    return 0;
}