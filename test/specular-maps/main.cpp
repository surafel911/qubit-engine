#include <cmath>
#include <string>
#include <vector>
#include <iostream>

#include <qubit/quad.hpp>
#include <qubit/engine.hpp>
#include <qubit/camera.hpp>

#include <qubit/opengl/ebo.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/vao.hpp>
#include <qubit/opengl/index.hpp>
#include <qubit/opengl/vertex.hpp>
#include <qubit/opengl/shader.hpp>
#include <qubit/opengl/texture.hpp>
#include <qubit/opengl/basic_renderer.hpp>
#include <qubit/opengl/element_renderer.hpp>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <loguru/loguru.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct plane_geometry
{
    glm::fmat4 model;

    static constexpr std::array<GLfloat, 48> vertices = {
         //     COORDINATES     /        COLORS             /    TexCoord    /       NORMALS     //
        -0.5f, 0.0f,  0.5f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 0.0f,		0.0f, 1.0f, 0.0f,
        -0.5f, 0.0f, -0.5f,		0.0f, 0.0f, 0.0f, 0.0f,		0.0f, 1.0f,		0.0f, 1.0f, 0.0f,
         0.5f, 0.0f, -0.5f,		0.0f, 0.0f, 0.0f, 0.0f,		1.0f, 1.0f,		0.0f, 1.0f, 0.0f,
	     0.5f, 0.0f,  0.5f,		0.0f, 0.0f, 0.0f, 0.0f,		1.0f, 0.0f,		0.0f, 1.0f, 0.0f
    };

    static constexpr std::array<GLushort, 6> elements = {
        0, 1, 2,
	    0, 2, 3
    };

    plane_geometry() : model(1.0f)
    { }
};

struct cube_geometry
{
    glm::fmat4 model;

    static constexpr std::array<GLfloat, 24> vertices = {
         //     COORDINATES     //
        -0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f,  0.5f
    };

    static constexpr std::array<GLushort, 36> elements = {
        0, 1, 2,
        0, 2, 3,
        0, 4, 7,
        0, 7, 3,
        3, 7, 6,
        3, 6, 2,
        2, 6, 5,
        2, 5, 1,
        1, 5, 4,
        1, 4, 0,
        4, 5, 6,
        4, 6, 7
    };
    
    cube_geometry() : model(1.0f)
    { }
};

int
main(int argc, char* argv[])
{
    qb::engine* e = qb::engine::get(qb::engine::config(
        argc, argv,
        qb::window::config("Hello World", glm::ivec2(1280, 720), false)
    ));

    glEnable(GL_DEBUG_OUTPUT);

    glEnable(GL_DEPTH_TEST);
    // glDepthFunc(GL_LESS);
    // glFrontFace(GL_CW);
    // glCullFace(GL_BACK);
    // glEnable(GL_CULL_FACE);
    glEnable(GL_PROGRAM_POINT_SIZE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
    qb::opengl::shader_manager& shader_manager = e->get_shader_manager();

    shader_manager.create_shader("default", std::make_pair(
            "test/resources/default.vertex.glsl",
            "test/resources/default.fragment.glsl"
        )
    );

    shader_manager.create_shader("specular", std::make_pair(
            "test/resources/specular.vertex.glsl",
            "test/resources/specular.fragment.glsl"
        )
    );

    shader_manager.create_shader("light", std::make_pair(
            "test/resources/light.vertex.glsl",
            "test/resources/light.fragment.glsl"
        )
    );

    plane_geometry plane_geometry;
    plane_geometry.model = glm::rotate(plane_geometry.model, glm::radians(90.0f), glm::fvec3(1.0f, 0.0f, 0.0f));
    plane_geometry.model = glm::scale(plane_geometry.model, glm::fvec3(0.3f));

    qb::opengl::element_renderer plane_renderer("specular",
        {
            {"pos", qb::opengl::vertex_data_type::float3, GL_FALSE},
            {"", qb::opengl::vertex_data_type::float4, GL_FALSE},
            {"texcoord", qb::opengl::vertex_data_type::float2, GL_FALSE},
            {"normal", qb::opengl::vertex_data_type::float3, GL_FALSE},
        }
    );
    
    plane_renderer.submit_vbo_data(plane_geometry.vertices.size(), plane_geometry.vertices.data());
    plane_renderer.submit_ebo_data(plane_geometry.elements.size(), plane_geometry.elements.data());

    cube_geometry cube_geometry;
    cube_geometry.model = glm::translate(cube_geometry.model, {0.0f, 0.0f, 0.2f});  
    cube_geometry.model = glm::scale(cube_geometry.model, glm::fvec3(0.1f));

    qb::opengl::element_renderer cube_renderer("light",
        {
            {"pos", qb::opengl::vertex_data_type::float3, GL_FALSE},
        }
    );

    cube_renderer.submit_vbo_data(cube_geometry.vertices.size(), cube_geometry.vertices.data());
    cube_renderer.submit_ebo_data(cube_geometry.elements.size(), cube_geometry.elements.data());

    qb::opengl::texture_manager& texture_manager = e->get_texture_manager();

    texture_manager.create_texture("plank", "test/resources/planks.png");
    texture_manager.create_texture("plank.specular", "test/resources/planks.specular.png");

    plane_renderer.get_shader().set_sampler_unit("sampler0","plank", 0);
    plane_renderer.get_shader().set_sampler_unit("sampler1", "plank.specular", 1);

    qb::third_person_camera camera({0.0f, 0.0f, 15.0f});

    glm::vec3 light_pos = {0.0f, 0.0f, 0.2f};
    glm::vec4 light_col = {1.0f, 1.0f, 1.0f, 1.0f};

    glClearColor(.07f, .13f, .17f, 1.0f);
    while (!e->get_window().should_close()) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        camera.input(e->get_time_step());

        std::array<glm::fmat4, 2> matrices = camera.get_matrices(glm::radians(90.0f), e->get_window().get_aspect(), 0.5f, 100.0f);

        plane_renderer.get_shader().set_uniform_data("model", plane_geometry.model);
        plane_renderer.get_shader().set_uniform_data("view", matrices[0]);
        plane_renderer.get_shader().set_uniform_data("proj", matrices[1]);
        plane_renderer.get_shader().set_uniform_data("light_pos", light_pos);
        plane_renderer.get_shader().set_uniform_data("light_col", light_col);
        plane_renderer.get_shader().set_uniform_data("camera_pos", camera.get_position());
        plane_renderer.get_shader().set_uniform_data("normal_matrix", glm::inverse(plane_geometry.model), GL_TRUE);

        plane_renderer.draw();

        // cube_renderer.get_shader().set_uniform_data("model", cube_geometry.model);
        // cube_renderer.get_shader().set_uniform_data("view", matrices[0]);
        // cube_renderer.get_shader().set_uniform_data("proj", matrices[1]);
        // cube_renderer.get_shader().set_uniform_data("light_col", light_col);

        // cube_renderer.draw();

        e->update();
    }

    return 0;
}