#version 460 compatibility

// in vec4 vcol;
in vec2 vtexcoord;
in vec3 vnormal;
in vec3 vposition;

uniform vec3 light_pos;
uniform vec4 light_col;
uniform vec3 camera_pos;

uniform sampler2D sampler0;
uniform sampler2D sampler1;

out vec4 fcol;

float
ambient_lighting()
{
    return 0.2;
}

float
diffuse_lighting()
{
    vec3 normal = normalize(vnormal);
    vec3 light_direction = normalize(light_pos - vposition);

    float diffuse = max(dot(normal, light_direction), 0.0f);

    return diffuse;
}

float
specular_lighting()
{
    // Increasing value will make the light more point like
    float power = 32;

    // Increasing value will increase intensity of specular light
    float specular_light = 0.25f;

    // Repeated code here is simply for explaination, not meant to be actually done.
    vec3 normal = normalize(vnormal);
    vec3 view_direction = normalize(camera_pos - vposition);
    vec3 light_direction = normalize(light_pos - vposition);
    vec3 reflection_direction = reflect(-light_direction, normal);

    float specular_amount = pow(max(dot(view_direction, reflection_direction), 0.0f), power);

    return specular_light * specular_amount;
}

void
main()
{
    float ambient = ambient_lighting();
    float diffuse = diffuse_lighting();
    float specular = specular_lighting();

    fcol = texture(sampler0, vtexcoord) * light_col * vec4(vec3(diffuse + ambient), 1.0f) + texture(sampler1, vtexcoord).r * specular;
}