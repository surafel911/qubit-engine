#version 460 compatibility

in vec3 pos;
in vec4 col;

out vec4 vcol;

void
main()
{
    vcol = col;
    gl_Position = vec4(pos, 1.0f);
}