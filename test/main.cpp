#include <cmath>
#include <string>
#include <vector>
#include <iostream>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <loguru/loguru.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <qubit/engine.hpp>
#include <qubit/camera.hpp>
#include <qubit/geometry.hpp>
#include <qubit/geometry_types.hpp>
#include <qubit/geometry_manager.hpp>

#include <qubit/opengl/ebo.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/vao.hpp>
#include <qubit/opengl/vertex.hpp>
#include <qubit/opengl/shader.hpp>
#include <qubit/opengl/element.hpp>
#include <qubit/opengl/texture.hpp>
#include <qubit/opengl/basic_renderer.hpp>
#include <qubit/opengl/element_renderer.hpp>

#include <qubit/opengl/shader_manager.hpp>
#include <qubit/opengl/texture_manager.hpp>

int
main(int argc, char* argv[])
{
#if 0
    qb::geometry_manager<qb::plane_geometry::plane_vertex_count, qb::plane_geometry::plane_element_count> plane_manager(
        qb::plane_geometry::plane_geometry_identity,
        qb::plane_geometry::plane_elements_identity
    );

    plane_manager.create_geometry();
    plane_manager.create_geometry();
    plane_manager.create_geometry();
    plane_manager.create_geometry();
    plane_manager.create_geometry();
    plane_manager.create_geometry();
    plane_manager.create_geometry();
    plane_manager.create_geometry();

    // const std::pair<const GLushort*, const GLsizei> elements = plane_manager.get_elements();
    // 
    // for(int i = 0; i < elements.second / qb::plane_geometry::plane_element_count; ++i) {
    //     for (int j = 0; j < qb::plane_geometry::plane_element_count; ++j) {
    //         std::cout << elements.first[i * qb::plane_geometry::plane_element_count + j] << std::endl;
    //     }
    //     std::cout << std::endl;
    // }

    qb::geometry_manager<qb::cube_geometry::cube_vertex_count, qb::cube_geometry::cube_element_count> cube_manager(
        qb::cube_geometry::cube_geometry_identity,
        qb::cube_geometry::cube_elements_identity
    );

    cube_manager.create_geometry();
    cube_manager.create_geometry();
    cube_manager.create_geometry();
    cube_manager.create_geometry();
    cube_manager.create_geometry();
    cube_manager.create_geometry();
    cube_manager.create_geometry();
    cube_manager.create_geometry();

    const std::pair<const GLushort*, const GLsizei> elements = cube_manager.get_elements();

    for(int i = 0; i < elements.second / qb::cube_geometry::cube_element_count; ++i) {
        for (int j = 0; j < qb::cube_geometry::cube_element_count; ++j) {
            std::cout << elements.first[i * qb::cube_geometry::cube_element_count + j] << std::endl;
        }
        std::cout << std::endl;
    }

#else

    qb::engine* e = qb::engine::get(qb::engine::config(
        loguru::Verbosity_WARNING, argc, argv,
        qb::window::config("Hello World", glm::ivec2(1280, 720), false)
    ));

    glEnable(GL_DEBUG_OUTPUT);

    glEnable(GL_DEPTH_TEST);
    // glDepthFunc(GL_LESS);
    // glFrontFace(GL_CW);
    // glCullFace(GL_BACK);
    // glEnable(GL_CULL_FACE);
    glEnable(GL_PROGRAM_POINT_SIZE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
    qb::opengl::shader_manager& shader_manager = e->get_shader_manager();

    shader_manager.create_shader("default", std::make_pair(
            "test/resources/default.vertex.glsl",
            "test/resources/default.fragment.glsl"
        )
    );

    shader_manager.create_shader("specular", std::make_pair(
            "test/resources/specular.vertex.glsl",
            "test/resources/specular.fragment.glsl"
        )
    );

    shader_manager.create_shader("light", std::make_pair(
            "test/resources/light.vertex.glsl",
            "test/resources/light.fragment.glsl"
        )
    );

    qb::plane_manager plane_manager;
    qb::id_t plane_id = plane_manager.create_geometry();

    glm::fmat4& plane_transform = plane_manager.find_transform(plane_id);
    plane_transform = glm::rotate(plane_transform, glm::radians(90.0f), glm::fvec3(1.0f, 0.0f, 0.0f));

    qb::opengl::element_renderer plane_renderer("specular",
        {
            {"pos",         qb::opengl::vertex_data_type::float3,   GL_FALSE},
            {"",            qb::opengl::vertex_data_type::float4,   GL_FALSE},
            {"texcoord",    qb::opengl::vertex_data_type::float2,   GL_FALSE},
            {"",            qb::opengl::vertex_data_type::float1,   GL_FALSE},
            {"normal",      qb::opengl::vertex_data_type::float3,   GL_FALSE},
        }
    );
    
    const std::pair<const float*, const GLsizei> plane_vertex_data = plane_manager.get_vertices();
    const std::pair<const qb::opengl::element_t*, const GLsizei> plane_element_data = plane_manager.get_elements();

    plane_renderer.submit_vbo_data(plane_vertex_data.first, plane_vertex_data.second);
    plane_renderer.submit_ebo_data(plane_element_data.first, plane_element_data.second);

    qb::cube_manager cube_manager;
    qb::id_t cube_id = cube_manager.create_geometry();

    glm::fmat4& cube_transform = cube_manager.find_transform(cube_id);
    cube_transform = glm::translate(cube_transform, glm::fvec3(0.0f, 0.0f, 0.2f));
    cube_transform = glm::rotate(cube_transform, glm::radians(90.0f), glm::fvec3(1.0f, 0.0f, 0.0f));
    cube_transform = glm::scale(cube_transform, glm::fvec3(0.1f));

    qb::opengl::element_renderer cube_renderer("light",
        {
            {"pos",     qb::opengl::vertex_data_type::float3,   GL_FALSE},
            {"",        qb::opengl::vertex_data_type::float4,   GL_FALSE},
            {"",        qb::opengl::vertex_data_type::float2,   GL_FALSE},
            {"",        qb::opengl::vertex_data_type::float1,   GL_FALSE},
            {"",        qb::opengl::vertex_data_type::float3,   GL_FALSE},
        }
    );

    const std::pair<const float*, const GLsizei> cube_vertex_data = cube_manager.get_vertices();
    const std::pair<const qb::opengl::element_t*, const GLsizei> cube_element_data = cube_manager.get_elements();

    cube_renderer.submit_vbo_data(cube_vertex_data.first, cube_vertex_data.second);
    cube_renderer.submit_ebo_data(cube_element_data.first, cube_element_data.second);

    qb::opengl::texture_manager& texture_manager = e->get_texture_manager();

    texture_manager.create_texture("plank", "test/resources/planks.png");
    texture_manager.create_texture("plank.specular", "test/resources/planks.specular.png");

    plane_renderer.get_shader().set_sampler_unit("sampler0","plank", 0);
    plane_renderer.get_shader().set_sampler_unit("sampler1", "plank.specular", 1);

    qb::third_person_camera camera({0.0f, 0.0f, 25.0f});

    glm::vec3 light_pos = {0.0f, 0.0f, 0.2f};
    glm::vec4 light_col = {1.0f, 1.0f, 1.0f, 1.0f};

    glClearColor(.07f, .13f, .17f, 1.0f);
    while (!e->get_window().should_close()) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        camera.input(e->get_time_step());

        std::array<glm::fmat4, 2> matrices = camera.get_matrices(glm::radians(90.0f), e->get_window().get_aspect(), 0.5f, 100.0f);

        plane_transform = plane_manager.find_transform(plane_id);

        plane_renderer.get_shader().set_uniform_data("model", plane_transform);
        plane_renderer.get_shader().set_uniform_data("view", matrices[0]);
        plane_renderer.get_shader().set_uniform_data("proj", matrices[1]);
        plane_renderer.get_shader().set_uniform_data("light_pos", light_pos);
        plane_renderer.get_shader().set_uniform_data("light_col", light_col);
        plane_renderer.get_shader().set_uniform_data("camera_pos", camera.get_position());
        plane_renderer.get_shader().set_uniform_data("normal_matrix", glm::inverse(plane_transform), GL_TRUE);
        plane_renderer.draw();

        cube_transform = cube_manager.find_transform(cube_id);

        cube_renderer.get_shader().set_uniform_data("model", cube_transform);
        cube_renderer.get_shader().set_uniform_data("view", matrices[0]);
        cube_renderer.get_shader().set_uniform_data("proj", matrices[1]);
        cube_renderer.get_shader().set_uniform_data("light_col", light_col);
        cube_renderer.draw();

        e->update();
    }

    return 0;

#endif
}