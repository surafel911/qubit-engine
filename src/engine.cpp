#include <qubit/engine.hpp>

#include <memory>
#include <cstdlib>
#include <iostream>

#include <glad/gl.h>
#include <GLFW/glfw3.h>
#include <loguru/loguru.hpp>

#include <qubit/window.hpp>
#include <qubit/glfw_context.hpp>

// TODO: Avoid having GL classes in main engine
#include <qubit/opengl/shader_manager.hpp>
#include <qubit/opengl/texture_manager.hpp>

namespace
{
    qb::engine::config c;

    void GLAPIENTRY
    MessageCallback( GLenum source,
                    GLenum type,
                    GLuint id,
                    GLenum severity,
                    GLsizei length,
                    const GLchar* message,
                    const void* userParam )
    {
        if (severity == 0x824c || severity == 0x9146) {
            LOG_F(ERROR, "%s type = 0x%x, severity = 0x%x, message = %s",
                (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "GL LOG:"),
                type, severity, message);
            std::exit(-1);
        }

        LOG_F(INFO, "%s type = 0x%x, severity = 0x%x, message = %s",
            (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "GL LOG:"),
            type, severity, message);
    }
}

qb::engine::config::config(const loguru::NamedVerbosity verbosity, int argc, char** argv, const window::config& window_config)
    :   argc(argc), verbosity(verbosity), argv(argv), window_config(window_config)
{ }

qb::engine::engine(config& config)
    :   _time_step(0.0f),
        _loguru_context(config.verbosity, config.argc, config.argv),
        _window(config.window_config)
{
    int version = gladLoadGL((GLADloadfunc)glfwGetProcAddress);

    if (GLAD_VERSION_MAJOR(version) != 4 && GLAD_VERSION_MINOR(version) != 6) {
        LOG_F(ERROR, "Failed to load OpenGL 4.6");
        std::exit(-1);
    }

    // TODO: Add more descriptive version + extension logging and checking.
    LOG_F(INFO, "Loaded OpenGL 4.6");
    
    glDebugMessageCallback(MessageCallback, 0);

    glfwSwapInterval(1);
}

void
qb::engine::calc_time_step()
{
    static float curr_time = 0.0f, begin_time = 0.0f;
    
    curr_time = glfwGetTime();
    this->_time_step = curr_time - begin_time;
    begin_time = curr_time;
}

qb::engine::~engine()
{
    gladLoaderUnloadGL();
    LOG_F(INFO, "Unloaded OpenGL 4.6");
}


// TODO: Come up with a better solution than this.
qb::engine*
qb::engine::get(const config& config)
{
    c = config;
    static qb::engine e(c);

    return &e;
}


qb::engine*
qb::engine::get()
{
    return get(c);
}

qb::window&
qb::engine::get_window()
{
    return this->_window;
}

qb::opengl::shader_manager&
qb::engine::get_shader_manager()
{
    return this->_shader_manager;
}

qb::opengl::texture_manager&
qb::engine::get_texture_manager()
{
    return this->_texture_manager;
}

const float
qb::engine::get_time_step() const
{
    return this->_time_step;
}

void
qb::engine::update()
{
    glfwPollEvents();
    glfwSwapBuffers(this->_window._window);

    this->calc_time_step();
}