#include <qubit/loguru_context.hpp>

#include <loguru/loguru.hpp>

qb::loguru_context::loguru_context(const loguru::NamedVerbosity verbosity, int argc, char** argv)
{
    loguru::init(argc, argv);

    // TODO: Change verbosity this way
    loguru::g_stderr_verbosity = verbosity;
}