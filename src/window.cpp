#include <qubit/window.hpp>

#include <string>
#include <iostream>

#include <glm/vec2.hpp>
#include <GLFW/glfw3.h>
#include <loguru/loguru.hpp>

qb::window::config::config(const std::string& title, const glm::ivec2& size, const bool fullscreen)
{
    this->title = title;
    this->size = size;
    this->fullscreen = fullscreen;
}

qb::window::window(const config& config) : _window(nullptr)
{
    glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE);

    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    const GLFWvidmode* vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

    this->_window = glfwCreateWindow(config.size.x, config.size.y, config.title.c_str(), NULL, NULL);
    if (!this->_window) {
        glfwTerminate();

        LOG_F(ERROR, "Failed to create GLFW window.");
        std::exit(-1);
    }

    glfwSetWindowPos(this->_window, (vidmode->width - config.size.x) / 2, (vidmode->height - config.size.y) / 2);

    glfwMakeContextCurrent(this->_window);
    glfwShowWindow(this->_window);

    LOG_F(INFO, "Created GLFW window.");
}

qb::window::~window()
{
    glfwDestroyWindow(this->_window);
    LOG_F(INFO, "Destroyed GLFW window.");
}

bool
qb::window::should_close() const
{
    return glfwWindowShouldClose(this->_window);
}

GLFWwindow*
qb::window::get_handle() const
{
    return this->_window;
}

const glm::ivec2
qb::window::get_size() const
{
    glm::ivec2 dims;
    glfwGetWindowSize(this->_window, &dims.x, &dims.y);

    return dims;
}

const float
qb::window::get_aspect() const
{
    const glm::ivec2 size = this->get_size();
    return static_cast<float>((float)size.x / (float)size.y);
}