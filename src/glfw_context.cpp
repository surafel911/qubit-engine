#include <qubit/glfw_context.hpp>

#include <cstdlib>

#include <GLFW/glfw3.h>
#include <loguru/loguru.hpp>

qb::glfw_context::glfw_context()
{
    if (!glfwInit()) {
        LOG_F(ERROR, "Failed to initialize GLFW.");
        std::exit(-1);
    }

    // TODO: More descriptive version + extension logging.
    DLOG_F(INFO, "Initializd GLFW library.");
    DLOG_F(INFO, "GLFW Version: %s", glfwGetVersionString());
}

qb::glfw_context::~glfw_context()
{
    glfwTerminate();
    DLOG_F(INFO, "Terminated GLFW library.");
}