#include <qubit/camera.hpp>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <qubit/engine.hpp>
#include <qubit/window.hpp>

float qb::controlable_camera::default_speed = 1.5f;

float qb::controlable_camera::default_sensitivity = 20.0f;

const float
qb::controlable_camera::get_default_speed()
{
    return controlable_camera::default_speed;
}

void
qb::controlable_camera::set_default_speed(const float speed)
{
    controlable_camera::default_speed = speed;
}

const float
qb::controlable_camera::get_default_sensitivity()
{
    return controlable_camera::default_sensitivity;
}

void
qb::controlable_camera::set_default_sensitivity(const float sensitivity)
{
    controlable_camera::default_sensitivity = sensitivity;
}

void
qb::first_person_camera::input(float time_step)
{ }

void
qb::third_person_camera::input(float time_step)
    {
        float rot_x, rot_y;
        double mouse_x, mouse_y;

        qb::window& window = qb::engine::get()->get_window();
        glm::ivec2 size = window.get_size();

        if (glfwGetKey(window.get_handle(), GLFW_KEY_W)) {
            this->_position += time_step * (this->_speed + 2.5f) * this->_orientation;
        }
        
        if (glfwGetKey(window.get_handle(), GLFW_KEY_A)) {
            this->_position += time_step * this->_speed * -glm::normalize(glm::cross(this->_orientation, this->_up));
        }

        if (glfwGetKey(window.get_handle(), GLFW_KEY_S)) {
            this->_position += time_step * (this->_speed + 2.5f) * -this->_orientation;
        }

        if (glfwGetKey(window.get_handle(), GLFW_KEY_D)) {
            this->_position += time_step * this->_speed * glm::normalize(glm::cross(this->_orientation, this->_up));
        }
        
        if (glfwGetKey(window.get_handle(), GLFW_KEY_SPACE)) {
            this->_position += time_step * this->_speed * this->_up;
        } else if (glfwGetKey(window.get_handle(), GLFW_KEY_LEFT_CONTROL)) {
            this->_position += time_step * this->_speed * -this->_up;
        }

        if (glfwGetMouseButton(window.get_handle(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
            glfwSetInputMode(window.get_handle(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

            if (this->_first_click) {
                glfwSetCursorPos(window.get_handle(), (size.x / 2), (size.y / 2));
                this->_first_click = false;
            }

            glfwGetCursorPos(window.get_handle(), &mouse_x, &mouse_y);

            rot_x = this->_sensitivity * (float)(mouse_y - (size.y / 2)) / size.y;
            rot_y = this->_sensitivity * (float)(mouse_x - (size.x / 2)) / size.x;

            glm::fvec3 newOrientation = glm::rotate(this->_orientation, glm::radians(-rot_x), glm::normalize(glm::cross(this->_orientation, this->_up)));

            if (abs(glm::angle(newOrientation, this->_up) - glm::radians(90.0f)) <= glm::radians(85.0f)) {
                this->_orientation = newOrientation;
            }

            // Rotates the Orientation left and right
            this->_orientation = glm::rotate(this->_orientation, glm::radians(-rot_y), this->_up);

            // Sets mouse cursor to the middle of the screen so that it doesn't end up roaming around
            glfwSetCursorPos(window.get_handle(), (size.x / 2), (size.y / 2));
        } else if (glfwGetMouseButton(window.get_handle(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) {
            // Unhides cursor since camera is not looking around anymore
            glfwSetInputMode(window.get_handle(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            // Makes sure the next time the camera looks around it doesn't jump
            this->_first_click = true;
        }
}