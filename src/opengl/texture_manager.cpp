#include <qubit/opengl/texture_manager.hpp>

#include <string>
#include <unordered_map>

#include <loguru/loguru.hpp>

#include <qubit/opengl/texture.hpp>

qb::opengl::texture&
qb::opengl::texture_manager::create_texture(const std::string& path)
{
    std::unordered_map<std::string, texture>::iterator it = 
        this->_texture_map.find(path);

    if (it != this->_texture_map.end()) {
        LOG_F(WARNING, "Texture with path (\"%s\") already exist.", path.c_str());

        return it->second;
    }
    
    std::pair<std::unordered_map<std::string, texture>::iterator, bool> result =
        this->_texture_map.emplace(path, path);

    if (!result.second) {
        LOG_F(ERROR, "Failed to insert texture into texture map with path (\"%s\").", path.c_str());

        std::exit(-1);
    }

    return result.first->second;
}

qb::opengl::texture&
qb::opengl::texture_manager::create_texture(const std::string& alias, const std::string& path)
{
    std::unordered_map<std::string, texture>::iterator it = 
        this->_texture_map.find(alias);

    if (it != this->_texture_map.end()) {
        LOG_F(WARNING,
            "Texture with alias (\"%s\") and path (\"%s\") already exist.",
            alias.c_str(), path.c_str()
        );

        return it->second;
    }
    
    std::pair<std::unordered_map<std::string, texture>::iterator, bool> result =
        this->_texture_map.emplace(alias, path);

    if (!result.second) {
        LOG_F(ERROR,
            "Failed to insert texture into texture map \
            with alias (\"%s\") and path (\"%s\").", alias.c_str(), path.c_str()
        );

        std::exit(-1);
    }

    return result.first->second;
}

void
qb::opengl::texture_manager::destroy_texture(const std::string& name)
{
    // FIXME: Replace hardcoded message with function macro later.
    if (this->_texture_map.erase(name) == 0) {
        LOG_F(WARNING,
            "No texture with name (\"%s\") found. \
            \"qb::opengl::shader_manager::destroy_shader\" will do nothing.",
            name.c_str()
        );
    }
}

qb::opengl::texture&
qb::opengl::texture_manager::find_texture(const std::string& name)
{
    std::unordered_map<std::string, texture>::iterator it = 
        this->_texture_map.find(name);

    if (it == this->_texture_map.end()) {
        LOG_F(ERROR, "Texure with name (\"%s\") not in texture map.", name.c_str());
        
        std::exit(-1);
    }

    return it->second;
}