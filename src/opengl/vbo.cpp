#include <qubit/opengl/vbo.hpp>

#include <vector>
#include <cstdlib>

#include <glad/gl.h>
#include <loguru/loguru.hpp>

const GLuint
qb::opengl::vbo::size(const GLuint count)
{
    return count * sizeof(GLfloat);
}

qb::opengl::vbo::vbo(vbo&& vbo)
    :   _usage(vbo._usage), _vbo(vbo._vbo), _count(vbo._count)
{
    vbo._vbo = 0;
    vbo._count = 0;
}

qb::opengl::vbo::vbo()
    :   _usage(GL_DYNAMIC_DRAW), _count(0)
{
    glCreateBuffers(1, &this->_vbo);
}

qb::opengl::vbo::vbo(const GLenum usage, const std::vector<float>& data)
    :   vbo(usage, data.size(), data.data())
{ }

qb::opengl::vbo::vbo(const GLenum usage, const GLuint count, const float* data)
    :   _usage(usage), _count(count)
{
    glCreateBuffers(1, &this->_vbo);
    glNamedBufferData(this->_vbo, vbo::size(this->_count), data, usage);
}

qb::opengl::vbo::~vbo()
{
    glDeleteBuffers(1, &this->_vbo);
}

const GLuint
qb::opengl::vbo::get_gl_object() const
{
    return this->_vbo;
}

const GLuint
qb::opengl::vbo::get_count() const
{
    return this->_count;
}

void
qb::opengl::vbo::submit_data(const GLuint offset, const GLuint count, const float* data)
{
    if (data == nullptr) {
        LOG_F(ERROR, "Cannot pass nullptr into vertex buffer object.");
        std::exit(-1);
    }

    if (this->_count < count) {
        this->_count = count;
        glNamedBufferData(this->_vbo, vbo::size(count), data, this->_usage);
    }

    glNamedBufferSubData(this->_vbo, offset, vbo::size(count), data);
}