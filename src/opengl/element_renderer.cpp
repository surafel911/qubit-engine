#include <qubit/opengl/element_renderer.hpp>

#include <vector>
#include <string>

#include <glad/gl.h>

#include <qubit/engine.hpp>

#include <qubit/opengl/vao.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/shader.hpp>

#include <qubit/engine.hpp>

void
qb::opengl::element_renderer::begin() const
{
    qb::opengl::vao::bind(this->_vao);
    qb::opengl::shader::bind(engine::get()->get_shader_manager().find_shader(this->_shader_alias));
}

inline void
qb::opengl::element_renderer::end() const
{
    qb::opengl::shader::unbind();
    qb::opengl::vao::unbind();
}

qb::opengl::element_renderer::element_renderer(const std::string& shader_alias, std::vector<vertex_buffer_layout> layouts)
    :   _shader_alias(shader_alias)
{
    this->_vao.submit_vbo(this->_vbo, engine::get()->get_shader_manager().find_shader(this->_shader_alias), layouts);
    this->_vao.submit_ebo(this->_ebo);
}

qb::opengl::vao&
qb::opengl::element_renderer::element_renderer::get_vao()
{
    return this->_vao;
}

qb::opengl::vbo&
qb::opengl::element_renderer::element_renderer::get_vbo()
{
    return this->_vbo;
}

qb::opengl::ebo&
qb::opengl::element_renderer::element_renderer::get_ebo()
{
    return this->_ebo;
}

qb::opengl::shader&
qb::opengl::element_renderer::get_shader()
{
    return engine::get()->get_shader_manager().find_shader(this->_shader_alias);
}

void
qb::opengl::element_renderer::element_renderer::submit_vbo_data(const std::vector<float>& data)
{
    this->_vbo.submit_data(0, data.size(), data.data());
}

void
qb::opengl::element_renderer::element_renderer::submit_vbo_data(const float* data, const GLuint count)
{
    this->_vbo.submit_data(0, count, data);
}

void
qb::opengl::element_renderer::submit_vbo_data(const GLuint offset, const float* data, const GLuint count)
{
    this->_vbo.submit_data(offset, count, data);
}

void
qb::opengl::element_renderer::element_renderer::submit_ebo_data(const std::vector<GLushort>& data)
{
    this->_ebo.submit_data(0, data.size(), data.data());
}

void
qb::opengl::element_renderer::element_renderer::submit_ebo_data(const GLushort* data, const GLuint count)
{
    this->_ebo.submit_data(0, count, data);
}

void
qb::opengl::element_renderer::submit_ebo_data(const GLuint offset, const GLushort* data, const GLuint count)
{
    this->_ebo.submit_data(offset, count, data);
}

 void
qb::opengl::element_renderer::draw(const GLenum mode)
{
    this->begin();
    glDrawElements(mode, this->_ebo.get_count(), GL_UNSIGNED_SHORT, 0);
    this->end();
}