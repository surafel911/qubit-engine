#include <qubit/opengl/shader_manager.hpp>

#include <string>
#include <cstdlib>
#include <unordered_map>

#include <loguru/loguru.hpp>

#include <qubit/opengl/shader.hpp>

const std::string
qb::opengl::shader_manager::make_alias(const std::pair<const std::string, const std::string>& paths)
{
    return (paths.first + ";" + paths.second);
}

const std::string
qb::opengl::shader_manager::make_alias(const std::string& vertex_path, const std::string& fragment_path)
{
    return shader_manager::make_alias(std::pair<const std::string, const std::string>(vertex_path, fragment_path));
}

qb::opengl::shader&
qb::opengl::shader_manager::create_shader(
    const std::string& alias,
    const std::pair<const std::string, const std::string>& paths
)
{
    std::unordered_map<std::string, shader>::iterator it = 
        this->_shader_map.find(alias);

    if (it != this->_shader_map.end()) {
        LOG_F(WARNING,
            "Shader with alias (\"%s\") and paths \
            (vertex: \"%s\", fragment: \"%s\") already exist.",
            alias.c_str(), paths.first.c_str(), paths.second.c_str()
        );

        return it->second;
    }

    std::pair<std::unordered_map<std::string, shader>::iterator, bool> result =
        this->_shader_map.emplace(alias, paths);

    if (!result.second) {
        LOG_F(ERROR,
            "Failed to insert shader into shader map \
            with alias (\"%s\") and paths (vertex: \"%s\", fragment: \"%s\") \
            already exist.",
            alias.c_str(), paths.first.c_str(), paths.second.c_str()
        );

        std::exit(-1);
    }

    return result.first->second;
}

qb::opengl::shader&
qb::opengl::shader_manager::create_shader(
    const std::string& alias,
    const std::string& vertex_path,
    const std::string& fragment_path
)
{
    return this->create_shader(alias, std::pair<const std::string, const std::string>(vertex_path, fragment_path));
}

void
qb::opengl::shader_manager::destroy_shader(const std::string& alias)
{
    // FIXME: Replace hardcoded message with function macro later.
    if (this->_shader_map.erase(alias) == 0) {
        LOG_F(WARNING,
            "No shader with alias (\"%s\") found. \
            \"qb::opengl::shader_manager::destroy_shader\" will do nothing.",
            alias.c_str()
        );
    }
}

qb::opengl::shader&
qb::opengl::shader_manager::find_shader(const std::string& alias)
{
    std::unordered_map<std::string, shader>::iterator it = 
        this->_shader_map.find(alias);

    if (it == this->_shader_map.end()) {
        LOG_F(ERROR, "Shader with alias (\"%s\") is not in shader map.", alias.c_str());
        
        std::exit(-1);
    }

    return it->second;
}