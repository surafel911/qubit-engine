#include <qubit/opengl/ebo.hpp>

#include <vector>
#include <cstdlib>

#include <glad/gl.h>
#include <loguru/loguru.hpp>

const GLuint
qb::opengl::ebo::size(const GLuint count)
{
    return count * sizeof(GLushort);
}

qb::opengl::ebo::ebo()
    :   _usage(GL_DYNAMIC_DRAW), _count(0)
{
    glCreateBuffers(1, &this->_ebo);
}

qb::opengl::ebo::ebo(ebo&& ebo)
    :   _usage(ebo._usage), _ebo(ebo._ebo), _count(ebo._count)
{
    ebo._ebo = 0;
    ebo._count = 0;
}

qb::opengl::ebo::ebo(const GLenum usage, const std::vector<GLushort>& data)
    :   ebo(usage, data.size(), data.data())
{ }

qb::opengl::ebo::ebo(const GLenum usage, const GLuint count, const GLushort* data)
    :   _usage(usage), _count(count)
{
    glCreateBuffers(1, &this->_ebo);
    glNamedBufferData(this->_ebo, ebo::size(this->_count), data, usage);
}

qb::opengl::ebo::~ebo()
{
    glDeleteBuffers(1, &this->_ebo);
}

const GLuint
qb::opengl::ebo::get_gl_object() const
{
    return this->_ebo;
}

const GLuint
qb::opengl::ebo::get_count() const
{
    return this->_count;
}

void
qb::opengl::ebo::submit_data(const GLuint offset, const GLuint count, const GLushort* data)
{
    if (data == nullptr) {
        LOG_F(ERROR, "Cannot pass nullptr into vertex buffer object.");
        std::exit(-1);
    }
    
    if (this->_count < count) {
        this->_count = count;
        glNamedBufferData(this->_ebo, ebo::size(this->_count), data, this->_usage);
    }

    glNamedBufferSubData(this->_ebo, offset, ebo::size(this->_count), data);
}