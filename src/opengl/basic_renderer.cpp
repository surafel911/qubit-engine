#include <qubit/opengl/basic_renderer.hpp>

#include <vector>
#include <string>

#include <glad/gl.h>

#include <qubit/engine.hpp>

#include <qubit/opengl/vao.hpp>
#include <qubit/opengl/vbo.hpp>
#include <qubit/opengl/shader.hpp>

#include <qubit/engine.hpp>

void
qb::opengl::basic_renderer::begin() const
{
    qb::opengl::vao::bind(this->_vao);
    qb::opengl::shader::bind(engine::get()->get_shader_manager().find_shader(this->_shader_alias));
}

inline void
qb::opengl::basic_renderer::end() const
{
    qb::opengl::shader::unbind();
    qb::opengl::vao::unbind();
}

qb::opengl::basic_renderer::basic_renderer(const std::string& shader_alias, std::vector<vertex_buffer_layout> layouts)
    :   _shader_alias(shader_alias), _stride(0)
{
    this->_stride = this->_vao.submit_vbo(this->_vbo, engine::get()->get_shader_manager().find_shader(this->_shader_alias), layouts);
}

qb::opengl::vao&
qb::opengl::basic_renderer::basic_renderer::get_vao()
{
    return this->_vao;
}

qb::opengl::vbo&
qb::opengl::basic_renderer::basic_renderer::get_vbo()
{
    return this->_vbo;
}

qb::opengl::shader&
qb::opengl::basic_renderer::get_shader()
{
    return engine::get()->get_shader_manager().find_shader(this->_shader_alias);
}

void
qb::opengl::basic_renderer::submit_data(const std::vector<float>& data)
{
    this->_vbo.submit_data(0, data.size(), data.data());
}

void
qb::opengl::basic_renderer::submit_data(const float* data, const GLuint count)
{
    this->_vbo.submit_data(0, count, data);
}

void
qb::opengl::basic_renderer::submit_data(const GLuint offset, const float* data, const GLuint count)
{
    this->_vbo.submit_data(offset, count, data);
}

void
qb::opengl::basic_renderer::draw(const GLenum mode)
{
    this->begin();
    glDrawArrays(mode, 0, this->_vbo.get_count() / (this->_stride / sizeof(GLfloat)));
    this->end();
}