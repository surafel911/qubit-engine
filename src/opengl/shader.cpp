#include <qubit/opengl/shader.hpp>

#include <string>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <fstream>
#include <unordered_map>
#include <loguru/loguru.hpp>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#include <qubit/engine.hpp>
#include <qubit/opengl/texture.hpp>
#include <qubit/opengl/texture_manager.hpp>

namespace
{
    static GLchar message[512];
}

const GLuint
qb::opengl::shader::create_shader(const std::string& path, const GLenum type)
{
    std::ifstream file(path);
    if (!file.is_open()) {
        LOG_F(ERROR, "Couldn't find shader file: (\"%s\").", path.c_str());
        file.close();
        opengl::shader::~shader();
        std::exit(-1);
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    file.close();

    const std::string src = buffer.str();
    const GLchar* code = src.c_str();

    const GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &code, NULL);
    glCompileShader(shader);

    GLint success;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, sizeof(message), NULL, message);
        LOG_F(ERROR, "Failed to compile shader: (\"%s\")\n%s.", path.c_str(), message);
        shader::~shader();
        std::exit(-1);
    }

    return shader;
}

// TODO: Change to only add things to cache
const GLint
qb::opengl::shader::find_attrib_location_from_cache(const std::string& name)
{
    auto it = this->_location_cache.find(name);
    if (it != this->_location_cache.end()) {
        return it->second;
    }

    GLint location = glGetAttribLocation(this->_program, name.c_str());
    this->_location_cache.emplace(name, location);
    return location;
}

const GLint
qb::opengl::shader::find_uniform_location_from_cache(const std::string& name)
{
    auto it = this->_location_cache.find(name);
    if (it != this->_location_cache.end()) {
        return it->second;
    }

    GLint location = glGetUniformLocation(this->_program, name.c_str());
    this->_location_cache.emplace(name, location);
    return location;
}

void
qb::opengl::shader::bind(const shader& shader)
{
    glUseProgram(shader._program);

    // DLOG_F(INFO, "Binding shader program (%u).", shader._program);
}

void
qb::opengl::shader::unbind()
{
    glUseProgram(0);

    // DLOG_F(INFO, "Unbinding shader program.");
}

qb::opengl::shader::shader(const std::pair<const std::string, const std::string>& paths)
    :   shader(paths.first, paths.second)
{ }

qb::opengl::shader::shader(const std::string& vertex_path, const std::string& fragment_path)
    : _vertex(0), _fragment(0), _program(0)
{
    this->_program = glCreateProgram();

    this->_vertex = this->create_shader(vertex_path, GL_VERTEX_SHADER);
    DLOG_F(INFO, "OpenGL vertex shader object created from path: (\"%s\").", vertex_path.c_str());

    this->_fragment = this->create_shader(fragment_path, GL_FRAGMENT_SHADER);
    DLOG_F(INFO, "OpenGL fragment shader object created from path: (\"%s\").", fragment_path.c_str());

    glAttachShader(this->_program, this->_vertex);
    glAttachShader(this->_program, this->_fragment);
    glLinkProgram(this->_program);

    GLint success;
    glGetProgramiv(this->_program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(this->_program, sizeof(message), NULL, message);
        LOG_F(ERROR, "Failed to link shader program: (\"%s\", \"%s\")\n%s.", vertex_path.c_str(), fragment_path.c_str(), message);
        std::exit(-1);
    }
    
    LOG_F(INFO, "OpenGL shaders attached and linked: (\"%s\", \"%s\").", vertex_path.c_str(), fragment_path.c_str());
}

qb::opengl::shader::~shader()
{
    LOG_F(INFO, "OpenGL shader and program deleted: vertex (%u), fragment (%u), program(%u).", this->_vertex, this->_fragment, this->_program);
    
    glDeleteShader(this->_vertex);
    glDeleteShader(this->_fragment);
    glDeleteProgram(this->_program);
}

const GLint
qb::opengl::shader::get_attrib_location(const std::string& name)
{
    GLint location = this->find_attrib_location_from_cache(name.c_str());
    if (location == -1) {
        LOG_F(ERROR, "Attribute (\"%s\") does not exist in shader program (%u). Check if attribute is used.", name.c_str(), this->_program);
        std::exit(-1);
    }

    return location;
}

const GLint
qb::opengl::shader::get_uniform_location(const std::string& name)
{
    GLint location = this->find_uniform_location_from_cache(name.c_str());
    if (location == -1) {
        LOG_F(ERROR, "Uniform (\"%s\") does not exist in shader program (%u). Check if uniform is used.", name.c_str(), this->_program);
        std::exit(-1);
    }

    return location;
}

void
qb::opengl::shader::set_sampler_unit(const std::string& name, const std::string& texture, const GLuint unit)
{
    shader::bind(*this);

    const GLuint texture_object = qb::engine::get()->get_texture_manager().find_texture(texture).get_gl_object();

    DLOG_F(INFO, "Binding OpenGL texture object (%u) to texture unit (%u) with uniform (%s).", texture_object, unit, name.c_str());
    glBindTextureUnit(unit, texture_object);
    glUniform1i(this->get_uniform_location(name), unit);

    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const float data)
{
    shader::bind(*this);

    glUniform1fv(
        this->get_uniform_location(name),
        1,
        &data
    );

    shader::unbind();   
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const glm::fvec1& data)
{
    shader::bind(*this);

    glUniform1fv(
        this->get_uniform_location(name),
        1,
        (float*)(&data)
    );

    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const glm::fvec2& data)
{
    shader::bind(*this);

    glUniform2fv(
        this->get_uniform_location(name),
        1,
        glm::value_ptr(data)
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const glm::fvec3& data)
{
    shader::bind(*this);

    glUniform3fv(
        this->get_uniform_location(name),
        1,
        glm::value_ptr(data)
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const glm::fvec4& data)
{
    shader::bind(*this);

    glUniform4fv(
        this->get_uniform_location(name),
        1,
        glm::value_ptr(data)
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const glm::fmat3& data, const GLboolean transpose)
{
    shader::bind(*this);

    glUniformMatrix3fv(
        this->get_uniform_location(name),
        1,
        transpose,
        glm::value_ptr(data)
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const glm::fmat4& data, const GLboolean transpose)
{
    shader::bind(*this);

    glUniformMatrix4fv(
        this->get_uniform_location(name),
        1,
        transpose,
        glm::value_ptr(data)
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const std::vector<glm::fvec1>& data)
{
    shader::bind(*this);

    glUniform1fv(
        this->get_uniform_location(name),
        data.size(),
        (float*)data.data()
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const std::vector<glm::fvec2>& data)
{
    shader::bind(*this);

    glUniform2fv(
        this->get_uniform_location(name),
        data.size(),
        (float*)data.data()
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const std::vector<glm::fvec3>& data)
{
    shader::bind(*this);

    glUniform3fv(
        this->get_uniform_location(name),
        data.size(),
        (float*)data.data()
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const std::vector<glm::fvec4>& data)
{
    shader::bind(*this);

    glUniform4fv(
        this->get_uniform_location(name),
        data.size(),
        (float*)data.data()
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const std::vector<glm::fmat3>& data, const GLboolean transpose)
{
    shader::bind(*this);

    glUniformMatrix3fv(
        this->get_uniform_location(name),
        data.size(),
        transpose,
        (float*)data.data()
    );
    
    shader::unbind();
}

void
qb::opengl::shader::set_uniform_data(const std::string& name, const std::vector<glm::fmat4>& data, const GLboolean transpose)
{
    shader::bind(*this);

    glUniformMatrix4fv(
        this->get_uniform_location(name),
        data.size(),
        transpose,
        (float*)data.data()
    );
    
    shader::unbind();
}