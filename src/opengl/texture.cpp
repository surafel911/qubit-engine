#include <qubit/opengl/texture.hpp>

#include <string>
#include <cstdlib>

#include <glad/gl.h>
#include <loguru/loguru.hpp>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#define STBI_NO_THREAD_LOCALS
#include <stb/stb_image.h>

qb::opengl::texture::texture(const std::string& path)
{
    if (path.empty()) {
        LOG_F(ERROR, "Couldn't find texture path: (\"%s\").", path.c_str());
        std::exit(-1);
    }

    LOG_F(INFO, "Opened texture file from path: (\"%s\").", path.c_str());

    int comp;
    stbi_set_flip_vertically_on_load(true);
    stbi_uc* data = stbi_load(path.c_str(), &this->_size.x, &this->_size.y, &comp, STBI_rgb_alpha);

    if (!data) {
        LOG_F(ERROR, "STB_Image Error: (\"%s\").", stbi_failure_reason());
        std::exit(-1);
    }

    LOG_F(INFO, "Loaded texture data path: (\"%s\").", path.c_str());

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    glCreateTextures(GL_TEXTURE_2D, 1, &this->_texture);

    this->set_texture_param(GL_TEXTURE_WRAP_S, GL_REPEAT);
    this->set_texture_param(GL_TEXTURE_WRAP_T, GL_REPEAT);
    this->set_texture_param(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    this->set_texture_param(GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTextureStorage2D(this->_texture, 1, GL_RGBA8, this->_size.x, this->_size.y);
    glTextureSubImage2D(this->_texture, 0, 0, 0, this->_size.x, this->_size.y, GL_RGBA, GL_UNSIGNED_BYTE, static_cast<stbi_uc*>(data));

    glGenerateTextureMipmap(this->_texture);

    LOG_F(INFO, "Created OpenGL texture object for path: (\"%s\").", path.c_str());
    
    // GLint swizzleMask[] = {GL_ONE, GL_ONE, GL_ONE, GL_ONE};
    // glTextureParameteriv(this->_texture, GL_TEXTURE_SWIZZLE_RGBA, swizzleMask);

    stbi_image_free(static_cast<stbi_uc*>(data));
}

qb::opengl::texture::~texture()
{
    GLuint tmp = this->_texture;
    glDeleteTextures(1, &this->_texture);
    LOG_F(INFO, "Deleted OpenGL texture object (%u).", tmp);
}

const glm::ivec2
qb::opengl::texture::get_size() const
{
    return this->_size;
}


const GLuint
qb::opengl::texture::get_gl_object() const
{
    return this->_texture;  
}

void
qb::opengl::texture::set_texture_param(const GLenum param, const GLint value) const
{
    glTextureParameteri(this->_texture, param, value);
}