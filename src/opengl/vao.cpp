#include <qubit/opengl/vao.hpp>

#include <cmath>
#include <array>
#include <string>
#include <vector>

#include <glad/gl.h>
#include <glm/glm.hpp>
#include <loguru/loguru.hpp>

#include <qubit/opengl/shader.hpp>
#include <qubit/opengl/ebo.hpp>
#include <qubit/opengl/vbo.hpp>

qb::opengl::vertex_buffer_layout::vertex_buffer_layout(
    const std::string& name,
    const vertex_data_type type,
    const GLboolean normalize
)
{
    this->name = name;
    this->type = type;
    this->normalize = normalize;
    this->divisor = 0;
}

qb::opengl::vertex_buffer_layout::vertex_buffer_layout(
    const std::string& name,
    const vertex_data_type type,
    const GLboolean normalize,
    const GLuint divisor
)
{
    this->name = name;
    this->type = type;
    this->normalize = normalize;
    this->divisor = divisor;
}

const int
qb::opengl::vao::calc_vertex_buffer_stride(
    const std::vector<vertex_buffer_layout>& layouts
)
{
    GLsizei stride = 0;
    for (vertex_buffer_layout layout : layouts) {
        stride += static_cast<GLsizei>(layout.type) * sizeof(GLfloat);
    }

    return stride;
}

void
qb::opengl::vao::set_vertex_vector_layout(
    shader& shader,
    const vbo& vbo,
    const vertex_buffer_layout& layout,
    const GLuint offset,
    const GLuint stride,
    const GLuint binding_index
)
{
        GLuint size = static_cast<GLuint>(layout.type);
        GLuint attrib_location = shader.get_attrib_location(layout.name);

        glVertexArrayVertexBuffer(
            this->_vao,
            attrib_location,
            vbo.get_gl_object(),
            offset,
            stride
        );
        
        DLOG_F(
            INFO,
            "glVertexArrayVertexBuffer(vao, %s, vbo, %d, %d)",
            layout.name.c_str(),
            offset,
            stride
        );

        glEnableVertexArrayAttrib(
            this->_vao,
            attrib_location
        );

        glVertexArrayAttribFormat(
            this->_vao,
            attrib_location,
            size,
            GL_FLOAT,
            layout.normalize,
            offset
        );

        DLOG_F(
            INFO,
            "glVertexArrayAttribFormat(vao, %s, %d, GL_FLOAT, %s, %d)",
            layout.name.c_str(),
            size,
            layout.normalize ? "GL_TRUE" : "GL_FALSE",
            offset
        );

        glVertexArrayAttribBinding(
            this->_vao,
            attrib_location,
            binding_index
        );

        glVertexArrayBindingDivisor(
            this->_vao,
            attrib_location,
            layout.divisor
        );
}

void
qb::opengl::vao::set_vertex_matrix_layout(
    shader& shader,
    const vbo& vbo,
    const vertex_buffer_layout& layout,
    const GLuint offset,
    const GLuint stride,
    const GLuint binding_index
)
{
    GLuint size = static_cast<GLuint>(layout.type);
    GLuint length = std::sqrt(static_cast<GLuint>(layout.type));
    GLuint attrib_location = shader.get_attrib_location(layout.name);

    for (int i = 0; i < length; ++i) {
        glVertexArrayVertexBuffer(
            this->_vao,
            attrib_location + i,
            vbo.get_gl_object(),
            (sizeof(GLfloat) * length * i),
            (sizeof(GLfloat) * size)
        );

        DLOG_F(
            INFO,
            "glVertexArrayVertexBuffer(vao, %s[%d], vbo, %d, %d)",
            layout.name.c_str(),
            i,
            offset,
            stride
        );


        glEnableVertexArrayAttrib(
            this->_vao,
            attrib_location + i
        );

        glVertexArrayAttribFormat(
            this->_vao,
            attrib_location + i,
            length,
            GL_FLOAT,
            layout.normalize,
            (sizeof(GLfloat) * length * i)
        );

        DLOG_F(
            INFO,
            "glVertexArrayAttribFormat(vao, %s[%d], %d, GL_FLOAT, %s, %d)",
            layout.name.c_str(),
            i,
            size,
            layout.normalize ? "GL_TRUE" : "GL_FALSE",
            offset
        );

        glVertexArrayAttribBinding(
            this->_vao,
            attrib_location + i,
            binding_index
        );

        glVertexArrayBindingDivisor(
            this->_vao,
            attrib_location + i,
            layout.divisor
        );
    }
}

const GLuint
qb::opengl::vao::set_vertex_buffer_layouts(
    shader& shader,
    const vbo& vbo,
    const std::vector<vertex_buffer_layout>& layouts
)
{
    GLuint offset = 0;
    GLuint stride = calc_vertex_buffer_stride(layouts);
    GLuint binding_index = shader.get_attrib_location(layouts[0].name);

    for (vertex_buffer_layout layout : layouts) {
        if (layout.name.empty())  {
            offset += static_cast<GLuint>(layout.type) * sizeof(GLfloat);
            continue;
        }

        switch (layout.type) {
            case vertex_data_type::float1:
            case vertex_data_type::float2:
            case vertex_data_type::float3:
            case vertex_data_type::float4:
                this->set_vertex_vector_layout(
                    shader,
                    vbo,
                    layout,
                    offset,
                    stride,
                    binding_index
                );
                break;
            case vertex_data_type::mat3:
            case vertex_data_type::mat4:
                this->set_vertex_matrix_layout(
                    shader,
                    vbo,
                    layout,
                    offset,
                    stride,
                    binding_index
                );
                break;
            default:
                // TODO: Handle this error better with macros or whatever
                LOG_F(ERROR, "Unkown qb::vertex_data_type passed into qb::vao::set_vertex_buffer_layouts");
                std::exit(-1);
        }

        offset += static_cast<GLuint>(layout.type) * 4;
    }

    return stride;
}

void
qb::opengl::vao::bind(const vao& vao)
{
    glBindVertexArray(vao._vao);
}

void
qb::opengl::vao::unbind()
{
    glBindVertexArray(0);
}

qb::opengl::vao::vao()
{
    glCreateVertexArrays(1, &this->_vao);
}

qb::opengl::vao::vao(vao&& vao)
    :  _vao(vao._vao)
{
    vao._vao = 0;
}

qb::opengl::vao::~vao()
{
    glDeleteVertexArrays(1, &this->_vao);
}

const GLuint
qb::opengl::vao::submit_vbo(
    const vbo& vbo,
    shader& shader,
    const std::vector<vertex_buffer_layout>& layouts
)
{
    return set_vertex_buffer_layouts(shader, vbo, layouts);
}

void
qb::opengl::vao::submit_ebo(const ebo& ebo)
{
    glVertexArrayElementBuffer(this->_vao, ebo.get_gl_object());
}

const GLuint
qb::opengl::vao::get_gl_object() const
{
    return this->_vao;
}